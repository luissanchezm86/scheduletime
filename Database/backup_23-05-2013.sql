USE [master]
GO
/****** Object:  Database [ScheduleTime]    Script Date: 05/23/2013 15:14:33 ******/
CREATE DATABASE [ScheduleTime] ON  PRIMARY 
( NAME = N'ScheduleTime', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\ScheduleTime.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ScheduleTime_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\ScheduleTime_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [ScheduleTime] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ScheduleTime].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ScheduleTime] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [ScheduleTime] SET ANSI_NULLS OFF
GO
ALTER DATABASE [ScheduleTime] SET ANSI_PADDING OFF
GO
ALTER DATABASE [ScheduleTime] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [ScheduleTime] SET ARITHABORT OFF
GO
ALTER DATABASE [ScheduleTime] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [ScheduleTime] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [ScheduleTime] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [ScheduleTime] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [ScheduleTime] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [ScheduleTime] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [ScheduleTime] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [ScheduleTime] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [ScheduleTime] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [ScheduleTime] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [ScheduleTime] SET  DISABLE_BROKER
GO
ALTER DATABASE [ScheduleTime] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [ScheduleTime] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [ScheduleTime] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [ScheduleTime] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [ScheduleTime] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [ScheduleTime] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [ScheduleTime] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [ScheduleTime] SET  READ_WRITE
GO
ALTER DATABASE [ScheduleTime] SET RECOVERY SIMPLE
GO
ALTER DATABASE [ScheduleTime] SET  MULTI_USER
GO
ALTER DATABASE [ScheduleTime] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [ScheduleTime] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'ScheduleTime', N'ON'
GO
USE [ScheduleTime]
GO
/****** Object:  Role [aspnet_Membership_BasicAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE ROLE [aspnet_Membership_BasicAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Membership_ReportingAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE ROLE [aspnet_Membership_ReportingAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Membership_FullAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE ROLE [aspnet_Membership_FullAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Personalization_BasicAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE ROLE [aspnet_Personalization_BasicAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Personalization_ReportingAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE ROLE [aspnet_Personalization_ReportingAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Personalization_FullAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE ROLE [aspnet_Personalization_FullAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Profile_BasicAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE ROLE [aspnet_Profile_BasicAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Profile_ReportingAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE ROLE [aspnet_Profile_ReportingAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Profile_FullAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE ROLE [aspnet_Profile_FullAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Roles_BasicAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE ROLE [aspnet_Roles_BasicAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Roles_ReportingAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE ROLE [aspnet_Roles_ReportingAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Roles_FullAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE ROLE [aspnet_Roles_FullAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_WebEvent_FullAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE ROLE [aspnet_WebEvent_FullAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Schema [aspnet_WebEvent_FullAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE SCHEMA [aspnet_WebEvent_FullAccess] AUTHORIZATION [aspnet_WebEvent_FullAccess]
GO
/****** Object:  Schema [aspnet_Roles_ReportingAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE SCHEMA [aspnet_Roles_ReportingAccess] AUTHORIZATION [aspnet_Roles_ReportingAccess]
GO
/****** Object:  Schema [aspnet_Roles_FullAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE SCHEMA [aspnet_Roles_FullAccess] AUTHORIZATION [aspnet_Roles_FullAccess]
GO
/****** Object:  Schema [aspnet_Roles_BasicAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE SCHEMA [aspnet_Roles_BasicAccess] AUTHORIZATION [aspnet_Roles_BasicAccess]
GO
/****** Object:  Schema [aspnet_Profile_ReportingAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE SCHEMA [aspnet_Profile_ReportingAccess] AUTHORIZATION [aspnet_Profile_ReportingAccess]
GO
/****** Object:  Schema [aspnet_Profile_FullAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE SCHEMA [aspnet_Profile_FullAccess] AUTHORIZATION [aspnet_Profile_FullAccess]
GO
/****** Object:  Schema [aspnet_Profile_BasicAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE SCHEMA [aspnet_Profile_BasicAccess] AUTHORIZATION [aspnet_Profile_BasicAccess]
GO
/****** Object:  Schema [aspnet_Personalization_ReportingAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE SCHEMA [aspnet_Personalization_ReportingAccess] AUTHORIZATION [aspnet_Personalization_ReportingAccess]
GO
/****** Object:  Schema [aspnet_Personalization_FullAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE SCHEMA [aspnet_Personalization_FullAccess] AUTHORIZATION [aspnet_Personalization_FullAccess]
GO
/****** Object:  Schema [aspnet_Personalization_BasicAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE SCHEMA [aspnet_Personalization_BasicAccess] AUTHORIZATION [aspnet_Personalization_BasicAccess]
GO
/****** Object:  Schema [aspnet_Membership_ReportingAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE SCHEMA [aspnet_Membership_ReportingAccess] AUTHORIZATION [aspnet_Membership_ReportingAccess]
GO
/****** Object:  Schema [aspnet_Membership_FullAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE SCHEMA [aspnet_Membership_FullAccess] AUTHORIZATION [aspnet_Membership_FullAccess]
GO
/****** Object:  Schema [aspnet_Membership_BasicAccess]    Script Date: 05/23/2013 15:14:33 ******/
CREATE SCHEMA [aspnet_Membership_BasicAccess] AUTHORIZATION [aspnet_Membership_BasicAccess]
GO
/****** Object:  Table [dbo].[aspnet_Applications]    Script Date: 05/23/2013 15:14:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_Applications](
	[ApplicationName] [nvarchar](256) NOT NULL,
	[LoweredApplicationName] [nvarchar](256) NOT NULL,
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[Description] [nvarchar](256) NULL,
PRIMARY KEY NONCLUSTERED 
(
	[ApplicationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[LoweredApplicationName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ApplicationName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [aspnet_Applications_Index] ON [dbo].[aspnet_Applications] 
(
	[LoweredApplicationName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
INSERT [dbo].[aspnet_Applications] ([ApplicationName], [LoweredApplicationName], [ApplicationId], [Description]) VALUES (N'/', N'/', N'3878b379-60a2-4b72-90e5-b29fa7adc316', NULL)
/****** Object:  Table [dbo].[aspnet_WebEvent_Events]    Script Date: 05/23/2013 15:14:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[aspnet_WebEvent_Events](
	[EventId] [char](32) NOT NULL,
	[EventTimeUtc] [datetime] NOT NULL,
	[EventTime] [datetime] NOT NULL,
	[EventType] [nvarchar](256) NOT NULL,
	[EventSequence] [decimal](19, 0) NOT NULL,
	[EventOccurrence] [decimal](19, 0) NOT NULL,
	[EventCode] [int] NOT NULL,
	[EventDetailCode] [int] NOT NULL,
	[Message] [nvarchar](1024) NULL,
	[ApplicationPath] [nvarchar](256) NULL,
	[ApplicationVirtualPath] [nvarchar](256) NULL,
	[MachineName] [nvarchar](256) NOT NULL,
	[RequestUrl] [nvarchar](1024) NULL,
	[ExceptionType] [nvarchar](256) NULL,
	[Details] [ntext] NULL,
PRIMARY KEY CLUSTERED 
(
	[EventId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Setup_RestorePermissions]    Script Date: 05/23/2013 15:14:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Setup_RestorePermissions]
    @name   sysname
AS
BEGIN
    DECLARE @object sysname
    DECLARE @protectType char(10)
    DECLARE @action varchar(60)
    DECLARE @grantee sysname
    DECLARE @cmd nvarchar(500)
    DECLARE c1 cursor FORWARD_ONLY FOR
        SELECT Object, ProtectType, [Action], Grantee FROM #aspnet_Permissions where Object = @name

    OPEN c1

    FETCH c1 INTO @object, @protectType, @action, @grantee
    WHILE (@@fetch_status = 0)
    BEGIN
        SET @cmd = @protectType + ' ' + @action + ' on ' + @object + ' TO [' + @grantee + ']'
        EXEC (@cmd)
        FETCH c1 INTO @object, @protectType, @action, @grantee
    END

    CLOSE c1
    DEALLOCATE c1
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Setup_RemoveAllRoleMembers]    Script Date: 05/23/2013 15:14:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Setup_RemoveAllRoleMembers]
    @name   sysname
AS
BEGIN
    CREATE TABLE #aspnet_RoleMembers
    (
        Group_name      sysname,
        Group_id        smallint,
        Users_in_group  sysname,
        User_id         smallint
    )

    INSERT INTO #aspnet_RoleMembers
    EXEC sp_helpuser @name

    DECLARE @user_id smallint
    DECLARE @cmd nvarchar(500)
    DECLARE c1 cursor FORWARD_ONLY FOR
        SELECT User_id FROM #aspnet_RoleMembers

    OPEN c1

    FETCH c1 INTO @user_id
    WHILE (@@fetch_status = 0)
    BEGIN
        SET @cmd = 'EXEC sp_droprolemember ' + '''' + @name + ''', ''' + USER_NAME(@user_id) + ''''
        EXEC (@cmd)
        FETCH c1 INTO @user_id
    END

    CLOSE c1
    DEALLOCATE c1
END
GO
/****** Object:  Table [dbo].[aspnet_SchemaVersions]    Script Date: 05/23/2013 15:14:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_SchemaVersions](
	[Feature] [nvarchar](128) NOT NULL,
	[CompatibleSchemaVersion] [nvarchar](128) NOT NULL,
	[IsCurrentVersion] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Feature] ASC,
	[CompatibleSchemaVersion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'common', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'health monitoring', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'membership', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'personalization', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'profile', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'role manager', N'1', 1)
/****** Object:  StoredProcedure [dbo].[aspnet_WebEvent_LogEvent]    Script Date: 05/23/2013 15:14:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_WebEvent_LogEvent]
        @EventId         char(32),
        @EventTimeUtc    datetime,
        @EventTime       datetime,
        @EventType       nvarchar(256),
        @EventSequence   decimal(19,0),
        @EventOccurrence decimal(19,0),
        @EventCode       int,
        @EventDetailCode int,
        @Message         nvarchar(1024),
        @ApplicationPath nvarchar(256),
        @ApplicationVirtualPath nvarchar(256),
        @MachineName    nvarchar(256),
        @RequestUrl      nvarchar(1024),
        @ExceptionType   nvarchar(256),
        @Details         ntext
AS
BEGIN
    INSERT
        dbo.aspnet_WebEvent_Events
        (
            EventId,
            EventTimeUtc,
            EventTime,
            EventType,
            EventSequence,
            EventOccurrence,
            EventCode,
            EventDetailCode,
            Message,
            ApplicationPath,
            ApplicationVirtualPath,
            MachineName,
            RequestUrl,
            ExceptionType,
            Details
        )
    VALUES
    (
        @EventId,
        @EventTimeUtc,
        @EventTime,
        @EventType,
        @EventSequence,
        @EventOccurrence,
        @EventCode,
        @EventDetailCode,
        @Message,
        @ApplicationPath,
        @ApplicationVirtualPath,
        @MachineName,
        @RequestUrl,
        @ExceptionType,
        @Details
    )
END
GO
/****** Object:  View [dbo].[vw_aspnet_Applications]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_Applications]
  AS SELECT [dbo].[aspnet_Applications].[ApplicationName], [dbo].[aspnet_Applications].[LoweredApplicationName], [dbo].[aspnet_Applications].[ApplicationId], [dbo].[aspnet_Applications].[Description]
  FROM [dbo].[aspnet_Applications]
GO
/****** Object:  Table [dbo].[aspnet_Users]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_Users](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[LoweredUserName] [nvarchar](256) NOT NULL,
	[MobileAlias] [nvarchar](16) NULL,
	[IsAnonymous] [bit] NOT NULL,
	[LastActivityDate] [datetime] NOT NULL,
PRIMARY KEY NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [aspnet_Users_Index] ON [dbo].[aspnet_Users] 
(
	[ApplicationId] ASC,
	[LoweredUserName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [aspnet_Users_Index2] ON [dbo].[aspnet_Users] 
(
	[ApplicationId] ASC,
	[LastActivityDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'271280d4-fdc8-49c3-8ce6-a35d0dbda0dc', N'alx6', N'alx6', NULL, 0, CAST(0x0000A18700F7F003 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'd8d5817c-4a21-42bf-abca-f8521e3cd1d0', N'aprado', N'aprado', NULL, 0, CAST(0x0000A186013912BB AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'95f624cd-ad78-47a7-a332-ad1712a2795f', N'Bridges00', N'bridges00', NULL, 0, CAST(0x0000A1C70102A181 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'825c6edd-dc2c-4b73-923c-98972d5bc2cf', N'calvarez05', N'calvarez05', NULL, 0, CAST(0x0000A185010549A7 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'9cb71e1e-099f-40ea-a376-18811fb5efd3', N'cromero', N'cromero', NULL, 0, CAST(0x0000A18C011130D6 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'2575858b-f8e8-4bbf-864e-88bc2354ed52', N'daniel bello', N'daniel bello', NULL, 0, CAST(0x0000A185016B2CDF AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'6fd6ae6d-4753-4f00-a392-a4d4aadcd154', N'dblancove', N'dblancove', NULL, 0, CAST(0x0000A18500EE7A8F AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'bba76e0c-1a15-4acd-886e-958b276174a7', N'eOrtiz', N'eortiz', NULL, 0, CAST(0x0000A18900E614CD AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'd4e754fd-e279-49c2-be96-0a3b860564c2', N'gbetancourt', N'gbetancourt', NULL, 0, CAST(0x0000A19700D2E3EF AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'b182901f-aef9-4116-bf3d-abb6a03b5d5b', N'jtorres', N'jtorres', NULL, 0, CAST(0x0000A1850137AB41 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'866fe705-6468-4df8-aeb6-9a757515e21c', N'lsanchez', N'lsanchez', NULL, 0, CAST(0x0000A1C7013E27EB AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', N'mgarcia', N'mgarcia', NULL, 0, CAST(0x0000A1970129710E AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'ea3a8f9b-09d2-4429-92ba-b2cb0f39feb5', N'msanchez', N'msanchez', NULL, 0, CAST(0x0000A186014FC7B1 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'2705a7ed-ffdf-4096-a0d3-8843fedfc64d', N'prueba', N'prueba', NULL, 0, CAST(0x0000A1860106C6DF AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', N'pskroche', N'pskroche', NULL, 0, CAST(0x0000A188017309BA AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'879b6128-dfb8-4e6c-8b68-09cc2e7dbdc9', N'pvillegas', N'pvillegas', NULL, 0, CAST(0x0000A1850166C3D7 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'b200e933-d80a-415c-95bb-d42564e415a7', N'rhmujica', N'rhmujica', NULL, 0, CAST(0x0000A185014E787B AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'2fc65ef5-bd2f-40ef-9864-73bcd34c3a7c', N'rpernia', N'rpernia', NULL, 0, CAST(0x0000A18501086C99 AS DateTime))
/****** Object:  StoredProcedure [dbo].[aspnet_UnRegisterSchemaVersion]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_UnRegisterSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128)
AS
BEGIN
    DELETE FROM dbo.aspnet_SchemaVersions
        WHERE   Feature = LOWER(@Feature) AND @CompatibleSchemaVersion = CompatibleSchemaVersion
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_CheckSchemaVersion]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_CheckSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128)
AS
BEGIN
    IF (EXISTS( SELECT  *
                FROM    dbo.aspnet_SchemaVersions
                WHERE   Feature = LOWER( @Feature ) AND
                        CompatibleSchemaVersion = @CompatibleSchemaVersion ))
        RETURN 0

    RETURN 1
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Applications_CreateApplication]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Applications_CreateApplication]
    @ApplicationName      nvarchar(256),
    @ApplicationId        uniqueidentifier OUTPUT
AS
BEGIN
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName

    IF(@ApplicationId IS NULL)
    BEGIN
        DECLARE @TranStarted   bit
        SET @TranStarted = 0

        IF( @@TRANCOUNT = 0 )
        BEGIN
	        BEGIN TRANSACTION
	        SET @TranStarted = 1
        END
        ELSE
    	    SET @TranStarted = 0

        SELECT  @ApplicationId = ApplicationId
        FROM dbo.aspnet_Applications WITH (UPDLOCK, HOLDLOCK)
        WHERE LOWER(@ApplicationName) = LoweredApplicationName

        IF(@ApplicationId IS NULL)
        BEGIN
            SELECT  @ApplicationId = NEWID()
            INSERT  dbo.aspnet_Applications (ApplicationId, ApplicationName, LoweredApplicationName)
            VALUES  (@ApplicationId, @ApplicationName, LOWER(@ApplicationName))
        END


        IF( @TranStarted = 1 )
        BEGIN
            IF(@@ERROR = 0)
            BEGIN
	        SET @TranStarted = 0
	        COMMIT TRANSACTION
            END
            ELSE
            BEGIN
                SET @TranStarted = 0
                ROLLBACK TRANSACTION
            END
        END
    END
END
GO
/****** Object:  Table [dbo].[aspnet_Paths]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_Paths](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[PathId] [uniqueidentifier] NOT NULL,
	[Path] [nvarchar](256) NOT NULL,
	[LoweredPath] [nvarchar](256) NOT NULL,
PRIMARY KEY NONCLUSTERED 
(
	[PathId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [aspnet_Paths_index] ON [dbo].[aspnet_Paths] 
(
	[ApplicationId] ASC,
	[LoweredPath] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Personalization_GetApplicationId]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Personalization_GetApplicationId] (
    @ApplicationName NVARCHAR(256),
    @ApplicationId UNIQUEIDENTIFIER OUT)
AS
BEGIN
    SELECT @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
END
GO
/****** Object:  Table [dbo].[aspnet_Roles]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_Roles](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
	[RoleName] [nvarchar](256) NOT NULL,
	[LoweredRoleName] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](256) NULL,
PRIMARY KEY NONCLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [aspnet_Roles_index1] ON [dbo].[aspnet_Roles] 
(
	[ApplicationId] ASC,
	[LoweredRoleName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_RegisterSchemaVersion]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_RegisterSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128),
    @IsCurrentVersion          bit,
    @RemoveIncompatibleSchema  bit
AS
BEGIN
    IF( @RemoveIncompatibleSchema = 1 )
    BEGIN
        DELETE FROM dbo.aspnet_SchemaVersions WHERE Feature = LOWER( @Feature )
    END
    ELSE
    BEGIN
        IF( @IsCurrentVersion = 1 )
        BEGIN
            UPDATE dbo.aspnet_SchemaVersions
            SET IsCurrentVersion = 0
            WHERE Feature = LOWER( @Feature )
        END
    END

    INSERT  dbo.aspnet_SchemaVersions( Feature, CompatibleSchemaVersion, IsCurrentVersion )
    VALUES( LOWER( @Feature ), @CompatibleSchemaVersion, @IsCurrentVersion )
END
GO
/****** Object:  Table [dbo].[aspnet_PersonalizationPerUser]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_PersonalizationPerUser](
	[Id] [uniqueidentifier] NOT NULL,
	[PathId] [uniqueidentifier] NULL,
	[UserId] [uniqueidentifier] NULL,
	[PageSettings] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [aspnet_PersonalizationPerUser_index1] ON [dbo].[aspnet_PersonalizationPerUser] 
(
	[PathId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [aspnet_PersonalizationPerUser_ncindex2] ON [dbo].[aspnet_PersonalizationPerUser] 
(
	[UserId] ASC,
	[PathId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aspnet_Profile]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_Profile](
	[UserId] [uniqueidentifier] NOT NULL,
	[PropertyNames] [ntext] NOT NULL,
	[PropertyValuesString] [ntext] NOT NULL,
	[PropertyValuesBinary] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aspnet_Membership]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_Membership](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Password] [nvarchar](128) NOT NULL,
	[PasswordFormat] [int] NOT NULL,
	[PasswordSalt] [nvarchar](128) NOT NULL,
	[MobilePIN] [nvarchar](16) NULL,
	[Email] [nvarchar](256) NULL,
	[LoweredEmail] [nvarchar](256) NULL,
	[PasswordQuestion] [nvarchar](256) NULL,
	[PasswordAnswer] [nvarchar](128) NULL,
	[IsApproved] [bit] NOT NULL,
	[IsLockedOut] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastLoginDate] [datetime] NOT NULL,
	[LastPasswordChangedDate] [datetime] NOT NULL,
	[LastLockoutDate] [datetime] NOT NULL,
	[FailedPasswordAttemptCount] [int] NOT NULL,
	[FailedPasswordAttemptWindowStart] [datetime] NOT NULL,
	[FailedPasswordAnswerAttemptCount] [int] NOT NULL,
	[FailedPasswordAnswerAttemptWindowStart] [datetime] NOT NULL,
	[Comment] [ntext] NULL,
PRIMARY KEY NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [aspnet_Membership_index] ON [dbo].[aspnet_Membership] 
(
	[ApplicationId] ASC,
	[LoweredEmail] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'd8d5817c-4a21-42bf-abca-f8521e3cd1d0', N'hrG7ZqrvZjJMnVd3HfN+YjxjW/A=', 1, N'qL317dbBZjdNYid2ZCrq0w==', NULL, N'aprado@inmotiongit.com', N'aprado@inmotiongit.com', NULL, NULL, 1, 0, CAST(0x0000A186013844A0 AS DateTime), CAST(0x0000A186013844A0 AS DateTime), CAST(0x0000A186013844A0 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'95f624cd-ad78-47a7-a332-ad1712a2795f', N'VPIx+b4U7SvG5cQCrvL7YadkkCk=', 1, N'b/K4sUtXOlCr3MUz94kBEw==', NULL, N'bridges00@gmail.com', N'bridges00@gmail.com', NULL, NULL, 1, 0, CAST(0x0000A1960132A20C AS DateTime), CAST(0x0000A1C6011B01B8 AS DateTime), CAST(0x0000A1960132A20C AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'825c6edd-dc2c-4b73-923c-98972d5bc2cf', N'hGMwDbK+bRFzlRhZZR4+1OsVpiQ=', 1, N'R/658P5AKlGDjv1J5YCsEw==', NULL, N'calvarez05@gmail.com', N'calvarez05@gmail.com', NULL, NULL, 1, 0, CAST(0x0000A18500EF3580 AS DateTime), CAST(0x0000A18501050AAF AS DateTime), CAST(0x0000A18500EF3580 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'9cb71e1e-099f-40ea-a376-18811fb5efd3', N'aG6WU/nCQI/AvIbXR2Mh06gQD3o=', 1, N'EclwNbeli/Oi4ndb/sg4WQ==', NULL, N'cromero@inmotiongit.com', N'cromero@inmotiongit.com', NULL, NULL, 1, 0, CAST(0x0000A18500F95A60 AS DateTime), CAST(0x0000A18C00D9B8E9 AS DateTime), CAST(0x0000A18500F95A60 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'2575858b-f8e8-4bbf-864e-88bc2354ed52', N'VUGeS4C8QMtDg9MRYfctduh6hRQ=', 1, N'6I3TOKGwNi35R+dCAii7aw==', NULL, N'danielvilla18@gmail.com', N'danielvilla18@gmail.com', NULL, NULL, 1, 0, CAST(0x0000A18500F6D5D8 AS DateTime), CAST(0x0000A18500F6D5D8 AS DateTime), CAST(0x0000A18500F6D5D8 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'6fd6ae6d-4753-4f00-a392-a4d4aadcd154', N'nlN+nLuAFm8FjzkiaAT7pvRJpCs=', 1, N'EyA17PThGrVsPBDHIMAlHQ==', NULL, N'dblancove@hotmail.com', N'dblancove@hotmail.com', NULL, NULL, 1, 0, CAST(0x0000A18500EE4670 AS DateTime), CAST(0x0000A18500EE4670 AS DateTime), CAST(0x0000A18500EE4670 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'bba76e0c-1a15-4acd-886e-958b276174a7', N'zeyHJG6b7qn32c4kYVYInin0ioM=', 1, N'L9ZQJtrPkEz6ZTs8q0BESA==', NULL, N'eduardoortizgil@outlook.com', N'eduardoortizgil@outlook.com', NULL, NULL, 1, 0, CAST(0x0000A18500F609F0 AS DateTime), CAST(0x0000A18500F609F0 AS DateTime), CAST(0x0000A18500F609F0 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'd4e754fd-e279-49c2-be96-0a3b860564c2', N'ts73xd5SPg/03/KvasjmTxSmihM=', 1, N'KfUB/ln5VNysytrFKynN/A==', NULL, N'guber_betancourt1374@hotmail.com', N'guber_betancourt1374@hotmail.com', NULL, NULL, 1, 0, CAST(0x0000A18500E81E44 AS DateTime), CAST(0x0000A19300D207C0 AS DateTime), CAST(0x0000A18500E81E44 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'2705a7ed-ffdf-4096-a0d3-8843fedfc64d', N'hy7TM1s7of7pKNh4goV0dP+qmKc=', 1, N'wbh1Gjco+QvQ1tkb2ZRxPg==', NULL, N'hola@hola', N'hola@hola', NULL, NULL, 1, 0, CAST(0x0000A18500F05BA4 AS DateTime), CAST(0x0000A18600FDF88E AS DateTime), CAST(0x0000A18500F05BA4 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'271280d4-fdc8-49c3-8ce6-a35d0dbda0dc', N'vKHwdndTvejguyLtDGH9zIa1ZZc=', 1, N'/zKHPqpxzpQ5pkuCpmohZw==', NULL, N'jgonzalez@inmotiongit.com', N'jgonzalez@inmotiongit.com', NULL, NULL, 1, 0, CAST(0x0000A18500EC3844 AS DateTime), CAST(0x0000A18700F68F55 AS DateTime), CAST(0x0000A18500EC3844 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'b182901f-aef9-4116-bf3d-abb6a03b5d5b', N'2z0Pea+/zO9smkrfT+YCQqGBzCw=', 1, N'DH5FDS+EaeNjgwYOjextMg==', NULL, N'jtorres@inmotiongit.com', N'jtorres@inmotiongit.com', NULL, NULL, 1, 0, CAST(0x0000A18501359BC4 AS DateTime), CAST(0x0000A1850137AB41 AS DateTime), CAST(0x0000A18501359BC4 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', N'liAuRWeUwi3D2Q8ip2vtYF44atQ=', 1, N'MycHk+2k9PLNaiRA0YjfEA==', NULL, N'mariat.garcia@outlook.com', N'mariat.garcia@outlook.com', NULL, NULL, 1, 0, CAST(0x0000A1870129C1C8 AS DateTime), CAST(0x0000A19601237120 AS DateTime), CAST(0x0000A187012F89E5 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'ea3a8f9b-09d2-4429-92ba-b2cb0f39feb5', N'3L4xhFdwVWB1pIlgrWfMAPJwoxM=', 1, N'zoiKpbcXz5D65laxFevjhQ==', NULL, N'masb90@gmail.com', N'masb90@gmail.com', NULL, NULL, 1, 0, CAST(0x0000A18500F0C3F0 AS DateTime), CAST(0x0000A18600CF6F73 AS DateTime), CAST(0x0000A18500F0C3F0 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', N'26RCkngu39ywD+CxJidKx302Q1w=', 1, N'BhasmKdQPzh0deI10d7GKg==', NULL, N'merpatty@hotmail.com', N'merpatty@hotmail.com', NULL, NULL, 1, 0, CAST(0x0000A1810146430C AS DateTime), CAST(0x0000A187016946D4 AS DateTime), CAST(0x0000A1810146430C AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'879b6128-dfb8-4e6c-8b68-09cc2e7dbdc9', N'dDytVxegbHYomonHBDRL3Ez081c=', 1, N'9R5XQQCBXUmM3JWcdswwTw==', NULL, N'pvillegas@inmotiongit.com', N'pvillegas@inmotiongit.com', NULL, NULL, 1, 0, CAST(0x0000A1850165DB54 AS DateTime), CAST(0x0000A1850165DB54 AS DateTime), CAST(0x0000A1850165DB54 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'866fe705-6468-4df8-aeb6-9a757515e21c', N'IeDYYblRatokV9l7CxmBa6ZU480=', 1, N'MycHk+2k9PLNaiRA0YjfEA==', NULL, N'qwe@qwe.qwe', N'qwe@qwe.qwe', NULL, NULL, 1, 0, CAST(0x0000A17A01345980 AS DateTime), CAST(0x0000A1C600E1F8E9 AS DateTime), CAST(0x0000A17A016E1547 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'b200e933-d80a-415c-95bb-d42564e415a7', N'Y2U79EO7t/GVse6FisT0Pwntyl0=', 1, N'mEJtvGB6lE4kbS4lqIOMmQ==', NULL, N'rhmujica@gmail.com', N'rhmujica@gmail.com', NULL, NULL, 1, 0, CAST(0x0000A185014C1930 AS DateTime), CAST(0x0000A185014E528B AS DateTime), CAST(0x0000A185014C1930 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'3878b379-60a2-4b72-90e5-b29fa7adc316', N'2fc65ef5-bd2f-40ef-9864-73bcd34c3a7c', N'wSEDpswScqr7NuYDaK+EYKYUwXQ=', 1, N'tqokvtAMeSs/3akKr7voIg==', NULL, N'rpernia@inmotiongit.com', N'rpernia@inmotiongit.com', NULL, NULL, 1, 0, CAST(0x0000A1850106DB68 AS DateTime), CAST(0x0000A18501082444 AS DateTime), CAST(0x0000A1850106DB68 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
/****** Object:  StoredProcedure [dbo].[aspnet_Paths_CreatePath]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Paths_CreatePath]
    @ApplicationId UNIQUEIDENTIFIER,
    @Path           NVARCHAR(256),
    @PathId         UNIQUEIDENTIFIER OUTPUT
AS
BEGIN
    BEGIN TRANSACTION
    IF (NOT EXISTS(SELECT * FROM dbo.aspnet_Paths WHERE LoweredPath = LOWER(@Path) AND ApplicationId = @ApplicationId))
    BEGIN
        INSERT dbo.aspnet_Paths (ApplicationId, Path, LoweredPath) VALUES (@ApplicationId, @Path, LOWER(@Path))
    END
    COMMIT TRANSACTION
    SELECT @PathId = PathId FROM dbo.aspnet_Paths WHERE LOWER(@Path) = LoweredPath AND ApplicationId = @ApplicationId
END
GO
/****** Object:  Table [dbo].[aspnet_PersonalizationAllUsers]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_PersonalizationAllUsers](
	[PathId] [uniqueidentifier] NOT NULL,
	[PageSettings] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PathId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Users_CreateUser]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Users_CreateUser]
    @ApplicationId    uniqueidentifier,
    @UserName         nvarchar(256),
    @IsUserAnonymous  bit,
    @LastActivityDate DATETIME,
    @UserId           uniqueidentifier OUTPUT
AS
BEGIN
    IF( @UserId IS NULL )
        SELECT @UserId = NEWID()
    ELSE
    BEGIN
        IF( EXISTS( SELECT UserId FROM dbo.aspnet_Users
                    WHERE @UserId = UserId ) )
            RETURN -1
    END

    INSERT dbo.aspnet_Users (ApplicationId, UserId, UserName, LoweredUserName, IsAnonymous, LastActivityDate)
    VALUES (@ApplicationId, @UserId, @UserName, LOWER(@UserName), @IsUserAnonymous, @LastActivityDate)

    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_RoleExists]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Roles_RoleExists]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(0)
    IF (EXISTS (SELECT RoleName FROM dbo.aspnet_Roles WHERE LOWER(@RoleName) = LoweredRoleName AND ApplicationId = @ApplicationId ))
        RETURN(1)
    ELSE
        RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_GetAllRoles]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Roles_GetAllRoles] (
    @ApplicationName           nvarchar(256))
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN
    SELECT RoleName
    FROM   dbo.aspnet_Roles WHERE ApplicationId = @ApplicationId
    ORDER BY RoleName
END
GO
/****** Object:  Table [dbo].[aspnet_UsersInRoles]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_UsersInRoles](
	[UserId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [aspnet_UsersInRoles_index] ON [dbo].[aspnet_UsersInRoles] 
(
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OfficeTime]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OfficeTime](
	[OfficeTimeId] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[ActionTime] [datetime] NOT NULL,
	[Enter] [bit] NOT NULL,
	[Comment] [varchar](200) NULL,
 CONSTRAINT [PK_OfficeTime] PRIMARY KEY CLUSTERED 
(
	[OfficeTimeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0ebc9c59-2934-4a58-94b5-0083e33d7112', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1BE00903210 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'32e33fe2-9920-4558-bfbb-0156ae53f53b', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A1780090BEB0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'04d71cdf-40c2-43d5-bdb1-01757b7c6d19', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C500C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'316f8aec-90f5-49c8-ad22-02db2c198606', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B800860880 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'dfd1e73c-1fba-4a91-bd68-032c77161cc8', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B3011BB8D0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'6dcddc8b-9caf-4d66-bebb-03f9dbf200e5', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A18E00DBBA00 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0da75f55-8dc3-432f-bd4a-04210a544ab5', N'd8d5817c-4a21-42bf-abca-f8521e3cd1d0', CAST(0x0000A1860083D600 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'325d6ac3-2295-454a-a30f-04b928f50b81', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A18800C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'5e6fdfbf-e160-4331-bc90-0534ff4a08bd', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1BF00D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'ca065fac-5ea8-4775-b765-054328d914ea', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', CAST(0x0000A18800D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'8501472d-b4b1-4de4-8aa2-073267115fa0', N'bba76e0c-1a15-4acd-886e-958b276174a7', CAST(0x0000A189008ED280 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'3b3f93a5-583c-4bcd-9eea-07fad7c22021', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1770136968C AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'150ec2e3-6b1d-4d65-832e-08080e0265d0', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A1960088C7A0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'798be7fb-edc0-4bc9-8a8d-08755aaedfa0', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18C008F811C AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'fa598c04-167f-46b1-8dac-08b354d69456', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A18C012E1FC0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0ad54d9a-cd6b-4746-934b-09040e7a4ba1', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1B7012E970D AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'4b043280-35ff-46e9-8c50-09d56faa3215', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C00134FD90 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'a80d73af-ce02-466d-bdc2-09dee23868fc', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B900C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'829d6f9a-2d58-4e42-9aef-0ab2fa85e3c9', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1A90126B550 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'269cb42e-2ff6-497c-acd3-0ac1a4874227', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1AA011E77F0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'caf70b77-502a-4f62-a21e-0bf062bb1c87', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A19600C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'45f11c88-c81a-45d3-8678-0ce86af16e02', N'271280d4-fdc8-49c3-8ce6-a35d0dbda0dc', CAST(0x0000A18700AC7C40 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'784dff5f-e5de-45e8-b6d6-0d0a70a21240', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17700D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'9b86a04d-9b8f-4188-a9b3-0d49cb32a2ec', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17F00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'7f8a58ae-383b-48a2-8e68-0dccc3904249', N'bba76e0c-1a15-4acd-886e-958b276174a7', CAST(0x0000A1860112A880 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'd119ab35-1560-4460-b5d1-0dfd4ca0cb31', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A19A0086DB70 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'8d20cb1c-a559-4846-8f87-0e5ad639a138', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1AB00D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'446047ea-4ba4-4203-978b-0e70bf90fa7a', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1BD01232340 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'11fab19e-eda6-4f8e-aee0-0ea5768fe5d2', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18601398B04 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'57611139-7feb-4a68-805a-0f2069ab270f', N'ea3a8f9b-09d2-4429-92ba-b2cb0f39feb5', CAST(0x0000A18600B77781 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'9116cb19-7876-4975-85e4-0f36544a4ee6', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A19700A470F1 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'2518e5f1-d5cd-4e38-9770-0fcc72a9ed26', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18600D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'f0acb39e-f943-461c-8178-109df712b09b', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18E00D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'9351b800-0fa0-4c2a-b3ca-10a8052c3bd4', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A19600C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'196e7604-5d0a-42e4-adfc-10cd3e65ccad', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1A8008CA000 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'2e68a00f-968c-47d2-a033-12e6ee89287a', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C600DBBA00 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'b51485e5-308f-4b20-a744-12e79023d4ad', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A19E00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'5b57c2ee-4328-4dc9-aed8-134e376d8aa0', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17E00BBDDC0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0171a7cf-edff-42da-a972-135fd1c63c25', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17800D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'12754574-8604-4615-bb9e-13c1e68d448a', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', CAST(0x0000A18101189EE7 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'cfa3259a-69c6-4895-b2d4-1569c19b0236', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B9011E77F0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'6693ebdd-e853-4259-8ea7-15c9067c71eb', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1A100C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'47f19c6f-5c84-4c0e-a221-16c242f1dbb9', N'271280d4-fdc8-49c3-8ce6-a35d0dbda0dc', CAST(0x0000A18500DE7920 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'69bceb21-e9b3-45d5-a422-16c708a6e02e', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A19E0083D600 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'a37a2132-5da5-4c2f-9111-1763469f095c', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B300C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0dfdc14c-0b98-46b7-ae2c-176da8355efc', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18C00D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'302ea8c3-cac0-4d98-b8aa-17a88a60bbb6', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1B600C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'24e32844-2461-4b82-a6ff-188d1a0a14b1', N'bba76e0c-1a15-4acd-886e-958b276174a7', CAST(0x0000A18800DBBA00 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'e91ab0e5-a2eb-42f9-9931-1b3292b31a07', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18800D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'c916d7f5-dbeb-4bd8-8f36-1b9d6479c688', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17700975630 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'58d12ff1-08ef-419a-a259-1be5ad013a48', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B700C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'e4707172-033d-450b-8120-1cced57adbc3', N'271280d4-fdc8-49c3-8ce6-a35d0dbda0dc', CAST(0x0000A185008CE650 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'8807876e-bab7-46bb-a71a-1e1f2e88148b', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17E00D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'600d3f9f-8403-45e7-beb3-1e2537bf0a57', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17901438C20 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'190f5b05-8afa-4312-9ac1-1e9ceaa14cc0', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B000C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'd5386114-e19d-4523-bd8b-1ef93aeebf54', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A195009B4B0A AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'4e22c015-e753-463d-a18c-1f260d14168d', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A189009698BC AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'483d4536-dd96-47c7-93be-1f46c008f824', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A18900DA2BA7 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'92f6b8d3-c9ae-40cd-9c6f-2002aab67aaa', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C5012482D0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'f72c0602-f6d7-4120-80cd-205962fd247b', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1780137BCB0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'8ffc1120-8adc-4ec7-8a7c-2067b0f78b5e', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A19400C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'd5f7b8f5-86c9-45fd-9d07-207504b86c38', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A18D00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'c5d04f20-8467-42bd-a519-2166719c1b37', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A18D00DBBA00 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0aa91f1e-cbcd-4faf-b717-221e6431a5e4', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A185008DB940 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'ecf4468f-8dc7-4406-9c14-22c68d311e19', N'271280d4-fdc8-49c3-8ce6-a35d0dbda0dc', CAST(0x0000A18600EC5234 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'3608324a-d940-4f80-9342-23794cf3a76b', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', CAST(0x0000A18600C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'afd1997f-27f8-43d7-bb02-239c0b4eee40', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17700C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'01807b62-2fee-4869-bdd8-23ae08ddef1b', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C100D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'51520d11-114a-4ec8-b67d-241785464714', N'd8d5817c-4a21-42bf-abca-f8521e3cd1d0', CAST(0x0000A18600C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'f21ede6a-33b6-4591-820f-24578980fcad', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A178011B2C30 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'9de41ed8-97b3-49b7-a7cd-247279d48e6e', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A19400DBEFA8 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'19f62e92-6ad2-4e8a-bce4-2495202f4311', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18D00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'92486cca-b086-4e3b-a419-24fd07894afa', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A1880128F8B6 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'2f24566a-1825-46da-a9f3-2502b134ff84', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1AB00888150 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'6f436d03-5928-465c-aa1c-25cb7f039271', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1740130DEE0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'25b48bc2-099a-4a09-823c-25d2549d8e91', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1BE00D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'4086fa28-b4b6-4776-b61e-260ca60f8f62', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A196012335C9 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'd32a67ec-b3ba-4dc7-865e-2661b8ee9a61', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B900D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'8501cb9a-416c-4ac0-91bf-26c56d6f3281', N'ea3a8f9b-09d2-4429-92ba-b2cb0f39feb5', CAST(0x0000A1850089E0E0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'4a2736f5-acff-4aba-adf5-2717d9c9086c', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A18700C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'7d8f468b-3b91-4bad-b3fb-2751d9bfc2ae', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1780095B050 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'8368a677-519f-4f86-b40e-27df4576db39', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18700D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'ade668cc-d05b-426e-9861-2812ee2e7dc7', N'b182901f-aef9-4116-bf3d-abb6a03b5d5b', CAST(0x0000A18500D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'213eb5d0-eb23-4fb0-affc-29260c1f63a9', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A179013AC220 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'24748122-adb4-4aa0-9ec9-29b31409e573', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A1930087E183 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'5ca278fc-fff0-4e5e-be1c-29e4767998ad', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1A400D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'fd4b60da-caf7-4b8f-bbf7-2ad425789167', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A19300C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'7adffbb4-6723-45bc-a554-2af6e65060ed', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18800DBA9D3 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'3921e54f-994f-41b2-8f4b-2b52c4f8d7b1', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18700DCDB96 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'a5b602b6-31a4-4f54-a992-2b8414aa91ef', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A18D008116E0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'1f12a0a8-a731-49e9-a1ec-2bd878c81abe', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18700C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'8e56341f-59e1-4610-8837-2bed3a1bb633', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B0012C3390 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'8ae149bb-1b30-49b9-a22b-2c50e5ef45b6', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', CAST(0x0000A18100895440 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'dbf1bf68-24b4-479e-8838-2c7a35c91669', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18D00D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'a721a995-d72d-4b14-8609-2e76f721ce6f', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18800C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'32097323-0357-4978-955b-2f3757c67802', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1BA00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'c9d4ca6f-9c70-45fc-8f17-2fc877760945', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', CAST(0x0000A18800C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'1d4d1481-2b7a-4953-8169-304f1ff6f1ed', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1A300C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'463848cb-8658-4e99-98c8-30e4d255ecea', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1BA011A9F90 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'dff9670e-f993-4b3d-ab2a-32398d7f9ec0', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1BD012A9FA0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'cfc2acef-bb98-4976-8844-32fc2e5fc64f', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17700C5C100 AS DateTime), 0, NULL)
GO
print 'Processed 100 total records'
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'45075eff-a5b7-4681-a3d3-335663ae527f', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', CAST(0x0000A1880098244C AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'3e12f9c7-1617-4eed-bc8a-35b29a719262', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A1870118B179 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'c9739a7d-3821-437f-bc1c-36a3402868a3', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A18C00D8DFEC AS DateTime), 1, N'llegue al medio dia por que estaba sacandole el pasaporte a mis hijas ')
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'49286b28-7bac-4e29-8e38-37471a431176', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A19700D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'324bb174-c80f-44fb-818d-377b17b45777', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17400D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'1f64c2fa-5546-443b-b462-37d1fa2b4724', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18D0130DEE0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'6d74220b-8782-4ce4-876a-37d22eca0503', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17A00DA5A70 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'565ac066-af26-4df9-a58e-383294025fc7', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1BA0084A8F0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'1de4e1d2-70b3-4473-8a28-383b7b2925a1', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A19300907860 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'ecb792ae-4ab0-4fd9-bfd0-3935c68416db', N'bba76e0c-1a15-4acd-886e-958b276174a7', CAST(0x0000A1870110BC50 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'a36c131a-4636-4660-8bd1-39c40191c5a8', N'271280d4-fdc8-49c3-8ce6-a35d0dbda0dc', CAST(0x0000A1860064CCB0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'e80a9010-7c79-45dc-b500-3a0e2fe07359', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B300853590 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'66f211fe-3edb-4ca0-9215-3afbec986f70', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18801087EF0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'9f38ab51-a300-463d-b851-3b0ec2ccbeb7', N'9cb71e1e-099f-40ea-a376-18811fb5efd3', CAST(0x0000A18C00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'415c68f7-0214-4db0-af06-3bd1af654cae', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A174008C1360 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'c5f647ab-2735-435b-8596-3c25608de91c', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A19B00841C50 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'f8f45781-5fce-4c3f-9649-3ca4da9009c0', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A18E00808A40 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'cdf8baf2-1026-493a-8313-3ca7c621065b', N'2575858b-f8e8-4bbf-864e-88bc2354ed52', CAST(0x0000A18500C595BB AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'f4732cd9-5d96-4e9f-99d7-3cb9b2a523f0', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B300D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'10a741ce-ce45-4a90-bebe-3cc4d18e6829', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A19C0084EF40 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'2fff7d9c-c72e-4502-8e2c-3cfc25b9a517', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1970144E575 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'4543ea93-28df-4c13-a937-3d6dd83ab910', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1B60092F130 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'42b2c668-e037-49e6-9fdf-3de78a219764', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A193011ABB43 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'9784d0c6-11f9-4086-9a2f-3f1d4849eb42', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1BD00DDA630 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'530ab832-9adc-4a2f-8477-3f48e69b6b59', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18000C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'b4a53a47-6a65-4ce8-bc63-3f504b571c36', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A186011B58F9 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'22e976a2-4821-4e6f-a77d-3fafba83e72c', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A180008FA570 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'a5e49afd-766a-4ec6-bdbe-3fea22984215', N'ea3a8f9b-09d2-4429-92ba-b2cb0f39feb5', CAST(0x0000A1860099CF00 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'72d5c3a5-3fb0-49ce-aaf5-40b3ec6c6b4c', N'bba76e0c-1a15-4acd-886e-958b276174a7', CAST(0x0000A18600D68210 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'b24d9c75-db76-4c79-b5eb-4101ce069a34', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17E00C22EF0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0ea4e942-3299-4383-a99a-4183deae7ac0', N'bba76e0c-1a15-4acd-886e-958b276174a7', CAST(0x0000A1880112A880 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'288cda86-a0fe-4cd3-a698-422b7548cf8b', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A195013A3580 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'7d8097a3-26da-4a71-9cad-429c093d95ae', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C6008A2730 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'77b7caad-f83f-45b0-a605-433afd221a02', N'bba76e0c-1a15-4acd-886e-958b276174a7', CAST(0x0000A186007E57C0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'46d81d60-cd29-497d-8e5f-44164cad0fe4', N'271280d4-fdc8-49c3-8ce6-a35d0dbda0dc', CAST(0x0000A186010C1CF3 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'4baa3fcc-3772-4656-97c3-44cc7acf43dc', N'9cb71e1e-099f-40ea-a376-18811fb5efd3', CAST(0x0000A18C008A2730 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'648acb3e-e39c-48ee-bae0-45432441de9c', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', CAST(0x0000A18600DA5A70 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'189dcbfe-f57e-482c-a7e0-4558fba283a5', N'825c6edd-dc2c-4b73-923c-98972d5bc2cf', CAST(0x0000A18500789330 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'd484e3c6-97ae-43fd-a93a-45c4763cb2b5', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1BF011D5EB0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'62552775-f7bf-43c4-b66d-46779fe7a933', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A194009C8E20 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'7f6eeb2c-b178-4be7-8efa-471de18eb388', N'b182901f-aef9-4116-bf3d-abb6a03b5d5b', CAST(0x0000A18500BC2410 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'6941f62d-dfb3-4d1a-b64c-47a5299c116d', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C600C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'f0d1baa9-d34d-4310-b8f5-48b3b7c14517', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1AF0121C3B0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'ecabf43e-52d7-4c12-9bca-48cb7a42aeb7', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B600860880 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'746a72eb-5179-4431-ba6c-495e37dd7277', N'bba76e0c-1a15-4acd-886e-958b276174a7', CAST(0x0000A18700C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'802b0b57-1fee-4af8-b638-4b26fb54efc9', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', CAST(0x0000A18700D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'51b832ef-2fc7-4059-9c4c-4b9f75c9240c', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18E00AFC800 AS DateTime), 1, N'Amanecí enfermo')
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'ec423da2-79ec-4b0a-81dd-4bb7add272f8', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A197007B98A0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'1ad75831-a5c1-4c6c-b94d-4c5aa7e1fdc5', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18500D79A78 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'8d754993-b8e0-49e6-9f18-4cd0d027591a', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A19700C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0698b20c-90c9-4e46-b729-4d188158e3a0', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1B800D9CDD0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'66ff2d23-d55b-4056-b93a-4dd8a7e1fb53', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A181013A39E6 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'821125da-625f-4daa-8fd8-4dfed8602d5d', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C000876810 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0873d43b-bb3a-41a3-8997-4f8cab7cad9a', N'bba76e0c-1a15-4acd-886e-958b276174a7', CAST(0x0000A18800C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'3b68aee8-d5c1-42f3-b0d9-4fb8b4c973a4', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1BE0130C4FD AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'f8a7be26-cd0e-4de8-866e-5029a4469fb5', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A19300C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'005fa471-c152-4b62-949f-51493a156e57', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C5013CF4A0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'6d8d9b7d-9f0f-4e32-8d01-514aa45f057d', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1A80127CE90 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'3ed2bf7c-de89-4526-aceb-51570aca6af1', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18901346FB7 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'19f2073c-9602-4387-a98f-516a127ee0be', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B600D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'9322d7e6-7054-4c53-ae19-516ce6e39bb5', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1A3011DA500 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'567e2a86-8fa5-4af5-a7b4-51d50ff7e6b2', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', CAST(0x0000A187011C4570 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0ebee2ac-e0d0-431d-8545-51f2ca84aba0', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A19301217D60 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'ad493467-335b-4b6c-9b63-527e84267ea4', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1AB01140810 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'15fe7aeb-ad5b-4628-adf8-52f822ec3a6c', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1AB00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'353b5fc4-7404-42a3-94bb-534f906bdaf0', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A19600DC8CF0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'e299bb89-c135-4ba5-9ec1-53798d12163d', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17800C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'a31fe639-3b42-4248-9411-539ae90d74f9', N'2705a7ed-ffdf-4096-a0d3-8843fedfc64d', CAST(0x0000A18600B3965E AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'a35e884e-043d-4abd-8d16-546cc9f7e21b', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A180011D5EB0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'fc29398c-e4c7-44f9-a9a7-5503f1629073', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18500C5E6BB AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'ee195ea0-059b-46cb-a54a-552b38e9a3dd', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1BD0092F130 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'deb88d82-da37-45a6-b0c4-55bda8a9c1a6', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C500D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'5c695fbe-176c-4ae8-a18f-569511d6f970', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17E00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'90d235eb-a286-4a42-a8fc-5798b592e588', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A19C011EF342 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'22d2de60-050f-44a1-af7f-57cdc26c33b8', N'271280d4-fdc8-49c3-8ce6-a35d0dbda0dc', CAST(0x0000A18500DFD8B0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'560255cc-010b-4aa2-81ea-5a1bf714f4c2', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18000970FE0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'86e85df0-b9bf-4dc0-b763-5b1b34028c69', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18C00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'bb1eeac7-0839-43c8-82f7-5c2e488a62bd', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1B700DE7920 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'142f3bdc-527e-4fd8-aa29-5c9873485581', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C1011F0490 AS DateTime), 0, N'aaa')
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'dab9a48a-4f3f-4689-9c90-5ca574726dfb', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', CAST(0x0000A18000C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'6975520f-ef9b-4d4d-878f-5d8528317542', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A180013543E0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'6f521e76-02d0-4eb3-8666-5dffc61e6c5f', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A19600C5F60B AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'129209b9-49c0-4012-9c2f-5e0814677745', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17401499700 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'82e3b3be-fb4c-42e7-a300-5e39f04130f5', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18800964B00 AS DateTime), 1, N'Llegue tarde por alcabalas en la vía. Fuga de presos')
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'50eb4734-133a-4d0e-941b-5f3bfa7e027e', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A181008DFF90 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'33632aed-a23e-4d10-a27e-5f8337d1fc51', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17700D8FAE0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'ee721f51-4e17-447f-97d3-5f91f6c208ba', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', CAST(0x0000A1870127AB99 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'cc331a73-3195-480b-8ba4-5fe53934755e', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1A900C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'39c7fb1e-c778-4b48-b7ec-6000aa521208', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A187008CCE31 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'29bbfdcd-0e8d-464d-abc7-613b04a526c1', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B70084A8F0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'fc5c3db5-5a09-4638-ab3f-618809446694', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A19300DC4357 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'4c3ad3f6-a294-43f8-8981-61e12f81e1fb', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A197011CD210 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'2a2b0388-5960-495b-b99a-6444b959b0a2', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B800C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'2e22f10a-ffb3-4d6e-9aaf-66247f518550', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1870137C430 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'9e86ffc4-d1f4-447d-a3e7-665e3f9fa17b', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1B700C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'70a6ceaa-14bc-4d94-88ee-6703d8f7bbca', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1960087F4B0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'a6025655-2c5e-466c-a1d9-672a87ee45ce', N'271280d4-fdc8-49c3-8ce6-a35d0dbda0dc', CAST(0x0000A186010B9BEB AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'42c336d3-a151-43dd-9216-67448e9f91b4', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C60121C3B0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'44eef664-d767-4f9e-bad1-68878f052aab', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A18E0134FD90 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'be5a65bc-fbb2-4966-be04-69abaefbcdf3', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1B800C53460 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'ed906091-6a12-483d-b5df-69afa5710268', N'bba76e0c-1a15-4acd-886e-958b276174a7', CAST(0x0000A187008D2EAE AS DateTime), 1, NULL)
GO
print 'Processed 200 total records'
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'af1b7285-187b-43ab-86ed-6aa660d325d1', N'271280d4-fdc8-49c3-8ce6-a35d0dbda0dc', CAST(0x0000A185012A8DB0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'5aad014a-4b4c-463f-90b1-6b7de6dd4266', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A18E00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'd864eb3b-a4e6-45de-9c46-6b990f459685', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1A4011AE5E0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'93c82c4f-1fe5-4397-998e-6bbc5102f292', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C6008FEBC0 AS DateTime), 1, N'habia cola')
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0e884a9c-d61e-4a19-86dd-6c1580a9361d', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', CAST(0x0000A18700C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'559e9d0e-63a4-43d0-b0ca-6c72b9281102', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', CAST(0x0000A187011813DF AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'8f6b8263-89b4-44e9-afdf-6ccaffb24945', N'bba76e0c-1a15-4acd-886e-958b276174a7', CAST(0x0000A185008116E0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'e9d639d4-dc0d-4d54-9ffd-6cdd74eb81b0', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18600C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0fa5474a-7a65-429b-a49b-6d0d96651acd', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', CAST(0x0000A180011AE5E0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'7d2521f2-5262-4f78-84de-6d2085a64b26', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', CAST(0x0000A18100C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'c7adc9e3-d5cf-4699-bd55-6d5254031977', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17400C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'8ea73c89-dc0c-4b0b-954d-6e0c8c532061', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', CAST(0x0000A18100DD1990 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'7f33bcc0-6031-43f7-8ccd-6e1efd4ea4eb', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A181011DEB50 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'3fe86cd8-efd7-4b0b-b727-6ecaa4143b62', N'b182901f-aef9-4116-bf3d-abb6a03b5d5b', CAST(0x0000A185008116E0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'c41d7296-252f-4af4-82fc-6f5f02a16b6a', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A19500C3AA33 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'a4c0e3ef-2a9a-45c5-8239-6f95f5a2ca0c', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', CAST(0x0000A180007B98A0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'dbcc25d7-c43d-45fa-851b-6ff6968a2501', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A19A01201DD0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'754bb70b-2dfe-48e2-a4c0-70f642740893', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A19E011B43EA AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'cb6ba29f-5805-4bf0-a3c7-718b8a67e437', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C000C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'a2c19519-f6ec-4f1b-859d-719232d46293', N'ea3a8f9b-09d2-4429-92ba-b2cb0f39feb5', CAST(0x0000A18600A4D56F AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'f957d369-7b5a-4a6e-b6a8-72b145fb5fa4', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1BF00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'94161604-fc1a-447a-9a03-74782ec11b65', N'271280d4-fdc8-49c3-8ce6-a35d0dbda0dc', CAST(0x0000A18600C5C73B AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'45e0bf00-0235-4b0d-8cd4-7526073ae5b9', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C000914B50 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'2fd34508-e06c-4fd3-a9e1-7572cda96fa5', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18C00937DD0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'951d0c60-50a1-4b43-997f-75794422d77e', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1BE00D75500 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'fffafab8-77db-4776-8982-757a6844d69e', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A188011C8BC0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'237007e7-a3f7-45cf-aeb9-77394fa33453', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1BF00DE7920 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'564f7902-f28b-4219-b23d-7756ea58466a', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18900E229A8 AS DateTime), 1, N'comentario editado')
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'29e41f1c-e1d1-4d01-980d-79691f1e2847', N'271280d4-fdc8-49c3-8ce6-a35d0dbda0dc', CAST(0x0000A186010D86E1 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'01bf9ce3-a328-4733-b385-79d63a07323e', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C000C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'80651e7a-3f1f-4b20-a4cf-7a3e17e95884', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1A400C6DA40 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'64b6495a-0f93-42cb-abb9-7b935027fd73', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17400D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'94d6c9e3-dcce-4eeb-9b97-7cf707c6c809', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1B801373010 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'3e680296-d9bf-4323-aa18-7cff8f383912', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1870098AA4D AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'be15f692-1d4d-4750-8a17-7e5e2088ea20', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A19B01188FB1 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'42e50189-fd92-4476-883d-7ecd125d53f2', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B200D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'a8255a9d-8049-4e60-857d-8006074acf2f', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17F0099CF00 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'7a1c292f-f135-4eb2-8c70-80275c65bd6c', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18D0134FCE1 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'b7426b51-06bf-4c7b-81fa-810fa895048c', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1850097B22F AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'76940fd0-575e-453b-8a41-813dedc44262', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A193012BED40 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'c08e5992-c555-47c6-86d7-814c51e663f2', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A19C00DC8CF0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'485da3f6-c989-4d1e-a7ff-81a0599c4d51', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A194008AFA20 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'1465468c-db48-42f1-9634-81b3e66292b3', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17F011BB8D0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'cf7ad178-e1f9-4b38-8892-8207cb79163b', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18800ACF8BB AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'c922e526-1ff0-40a9-9cfb-825adb1438b3', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A1890122EF86 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'be09c70d-1050-49f9-b570-8275483898ba', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1850132A667 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'627c03bb-afad-44ef-a999-82958c8dbde2', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1B900940A70 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'1f5e51f1-de75-48bb-968c-82a0a75102c9', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18D00DEBC96 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'7b283b42-6e72-4ba1-838d-82d060dd09ef', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1BD00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'd37832d3-d616-4dd7-8412-84387eb94e90', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17900926490 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'4de7f4ee-f464-4910-8b99-84666d66f7bd', N'ea3a8f9b-09d2-4429-92ba-b2cb0f39feb5', CAST(0x0000A18600B8F925 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'17b68e7c-b9fc-46b0-8456-84e6d716ca49', N'ea3a8f9b-09d2-4429-92ba-b2cb0f39feb5', CAST(0x0000A1860085C230 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'31109262-7566-4c2c-a869-8538b5ad96f1', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17A00D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'676e8c7a-ae64-4768-b9fd-857c60cc71a7', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A19500DB2D60 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'59e32180-6e13-4da7-a6aa-85fc9d0615c2', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', CAST(0x0000A186009C8E20 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'32d5fce9-2e4d-4ead-9d91-85fe6f799931', N'bba76e0c-1a15-4acd-886e-958b276174a7', CAST(0x0000A188007F7100 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'2bd93337-57bc-438e-be43-86555186d9e5', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18900D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'c4d1e4e6-efa9-4072-b8f7-89701d090abf', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17900C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'296259a0-0a56-449e-90a0-89a337771076', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18D00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'8c07b8e4-3dad-4e9a-8383-8a7dbab110db', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C001206420 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'04bfa0f4-e3e7-4325-b761-8a998bbe7ca5', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C40118F9B0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'a4054329-507a-4141-aa89-8ab65a5c3da5', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', CAST(0x0000A18000D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'23b305a6-caf7-4154-9546-8ba452e42903', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17400B54640 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'e95375c2-8857-4e69-a23d-8bc41f45c811', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18600D97AC2 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0bf90391-c613-47be-b78e-8c0cacb9897e', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18C01379D03 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'3f2409cf-2503-41cd-8416-8d0863f3f4c7', N'b200e933-d80a-415c-95bb-d42564e415a7', CAST(0x0000A18500C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'576e331c-4d13-4fc8-9c4d-8d729e1d39ae', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18100D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'7bb01bb1-8626-4286-b408-8d9ab04583dc', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C100C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'16e861ce-7ef5-42a6-ba45-8fa1ca4e08bf', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A19A00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'b9fe6367-8194-4e93-bad2-91351d72ca93', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17F00D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'52839bbe-0595-41fc-80fe-91732abdbaab', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A186009706C4 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'046dfc7c-5558-4996-b95f-91a84e49ace8', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17A00EC34C0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0fc9356b-5c05-4736-934e-923ff2555b17', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1BF008F18D0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'2bc0439b-e8b2-4107-a8f0-926768d7f4e9', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B600C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'4ffc5ca6-e64c-4b44-895a-93742d74cf5f', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C000DD1990 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'efdb8a3b-c160-4219-a720-94a27fbd0f40', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A185011C721F AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'a71a6226-4871-4c08-bc94-954196880443', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1BE00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'c8d4e4f7-f065-4a41-95a0-954da653e3b3', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C700D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'70337fb5-c2fe-4576-b131-9668ccf059d8', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A188013D3C39 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'eb0a6aa5-fddd-4136-b9a1-96900f1c4e25', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C700914B50 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'4319c8d2-85ac-4abd-bf66-96d6e3677b10', N'271280d4-fdc8-49c3-8ce6-a35d0dbda0dc', CAST(0x0000A186010F0577 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'aa1e945a-410b-43e4-a45b-96e42cf0ba2a', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18E013E9A80 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0b2870bb-df64-4d07-b744-97192b8f5338', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C100864ED0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'e4808116-931e-4c21-87d8-9830aacd6ccd', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17F00E3F760 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0f3e3dbb-8d83-4d13-8be6-983ba9f33b83', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18C00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'2652561e-d39c-4a6a-913e-9920f3f0ff77', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18D00886062 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'1ef1e2da-fb9a-48ec-8ac2-9933d017ed31', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1BE01194000 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0cb9732c-6fc9-482e-af0c-9999962e7960', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1B600D70EB0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'12fe9c18-d75f-4cf6-8768-99de40d80925', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B7011A9F90 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'5ab7109a-14fa-4d7a-bbab-9ab581812fbb', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C400C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'9762418f-cc54-40fb-873f-9b3b93d2dbc7', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17800DBBA00 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'74134e27-dcca-4439-b3d7-9c15b208e528', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B000D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'ed09bd4d-6fee-4826-9177-9cdb19367424', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B2008C1360 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'895755b4-a309-4188-a28f-9d5e622fe568', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1B80099CF00 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'f4bb2adf-db2a-4d91-89cd-9db6d1187978', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18C012D4806 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'3a97b1bb-d366-45b3-9a88-9e2cbabba816', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18000C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'1ccd0fd3-6d08-4056-bc71-9e42e10af304', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17900D8FAE0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'43d22aea-953a-4f4c-8ebb-9ee7078c1618', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17F00C53460 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'6c83b6dd-e424-4b44-815c-a03204a9662e', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18900C396B8 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'fce273b0-f77d-4c2f-8003-a094ee98b388', N'bba76e0c-1a15-4acd-886e-958b276174a7', CAST(0x0000A18700D8FAE0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'c7646677-118a-4840-ad24-a10e4af3ef85', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18900B47350 AS DateTime), 1, N'Visita al pediatra por consulta y vacuna')
GO
print 'Processed 300 total records'
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'73ab143a-b772-47d0-a9cf-a13f2dcc9228', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1A800D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'38d4a29a-66e3-44ac-8811-a29cad468e10', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1BA0139DF81 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'57eebbb3-37f4-49c4-90dc-a474c39197c1', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', CAST(0x0000A186011826C0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'ce1f83a3-e5fa-432b-9c10-a5169a362139', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18E010DEC56 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'3f8f70bc-e701-4d2a-84cc-a65b0f4e961f', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18800C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'539e62c0-df8c-4073-8e23-a6db55b14e90', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C700876810 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'276ca163-0adb-4dda-a387-a760cc89067d', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C000D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'24c88e18-f092-442a-8315-a78ba1749515', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A19B00D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'112d7364-4f63-41d5-acf1-a7dcd15c9ce3', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A19A00D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'6055e9b7-b599-4d4c-8968-a82c7c1b4f33', N'2575858b-f8e8-4bbf-864e-88bc2354ed52', CAST(0x0000A1850120BF57 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'3bec1d13-9bb0-40f1-a97f-a87919e60c55', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A188010E89D0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'94afbb09-dca7-4867-9a7d-a8a83c9087dc', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1B601487A02 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'8c806b0e-09a2-4f32-bfdd-aa80ad53b513', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B8011A9F90 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'04545959-fe3d-46bd-82d9-aa8ca01d9192', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1940145AF9F AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'261a8100-2fdb-4b64-8324-ad218556df8f', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A19E00D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'e2892f86-42df-475a-8756-add4ca36ae00', N'b200e933-d80a-415c-95bb-d42564e415a7', CAST(0x0000A18500D8FAE0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'460d3759-e823-4a99-9243-ae885ae0f164', N'b182901f-aef9-4116-bf3d-abb6a03b5d5b', CAST(0x0000A18500D79B50 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'fe0d3166-671c-445c-ad31-b01443b5392e', N'271280d4-fdc8-49c3-8ce6-a35d0dbda0dc', CAST(0x0000A18601362F84 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'df1d10d1-6848-4abf-8d4a-b02ebb510030', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18500C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'c1bbd412-9a39-44d4-a660-b05e9b4f9f16', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18100C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'2746875c-aff8-46b3-bb62-b0a8acbb89ef', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A19600D8FAE0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'cd6ef313-175d-4202-9937-b17a7f771a2b', N'9cb71e1e-099f-40ea-a376-18811fb5efd3', CAST(0x0000A18C00D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'32b810ea-a180-4f95-b43f-b1ab632d0b2a', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1A300D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'7c007a69-5af2-4d7d-ad0d-b1db2151c303', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C500C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0efce861-3db4-4d6f-958a-b25dd95cff05', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18600C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'45b101f0-7883-41fa-be9a-b408a3945a91', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B2012D9320 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'16a3e13d-5074-4b2f-ab7e-b472bae07394', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A177011C8BC0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'4c62bfb1-8d7b-46ef-98dd-b5585eac786d', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A18700DA1420 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'c4fcab6b-51ae-445a-b588-b5b8cea3ade8', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C400C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'b1a365c7-2692-43a8-a3da-b5f0cf6a243a', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1A10107AC00 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'795e37c2-a762-42cf-b209-b66baa65781c', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A19700E510A0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0000d533-1802-471a-a66c-b6e295fc3c6f', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1A30084A8F0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'6c1da91a-71fe-46fe-82f1-b89ec19678ce', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1B9013A5FA5 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'7f4d7740-73ca-4288-9d90-b8f7ceb75e75', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17900D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'c468c245-216e-43da-80df-b914ad07e646', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17F0137BCB0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'be383b31-b13c-49c9-9354-b920fd0ad03d', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17A00970FE0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'31ad78d2-e355-43de-9264-b9a6f8839c0a', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1A100D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'7f1357c2-a849-457e-a307-ba22ae1355a6', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1A900D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'a5005853-ed6e-4c1b-a723-bb662f94ce21', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1BD00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0ec70f02-1ce7-4879-8819-bb92a4ad848e', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C40095B050 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'74bdf056-d0a2-4f06-a059-bbf7af140964', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1BF00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'196356c5-88a9-4291-a3f1-bc8e9d764ec3', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A18D01339E00 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'353f985a-3291-44ed-aa0b-bcb245c4c188', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B800D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'27f70c15-549a-494a-b6b6-bce491193fb1', N'5875e4f3-2eed-4ed5-af5f-55df96c40271', CAST(0x0000A187007B0C00 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'986b3f7c-f48d-4d1d-8dcf-bce8c691847d', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A19300DCD340 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'bc2454f2-ccc1-401e-8e2e-bf46b570e68f', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A174010FE960 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'af89f416-1861-4fc9-b586-bfca136f5bfe', N'bba76e0c-1a15-4acd-886e-958b276174a7', CAST(0x0000A18500DA56C4 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'87a21496-9a08-405f-998f-c156da9eb9f2', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B6011A9F90 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'b72b0ad4-b22f-46dc-b87c-c2d4de77cc99', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A193009B2E90 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0fec18d8-3853-424d-a89a-c3f304d59bf8', N'2575858b-f8e8-4bbf-864e-88bc2354ed52', CAST(0x0000A18500D6F13F AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'a530ca06-15f7-4132-8b61-c414fcedfafe', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A19600DC8CF0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'15851d6e-f77a-455b-b9d7-c42610bd16b2', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A19300D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'7b4cef36-6bbf-429a-a9aa-c42d1e50eb77', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18700C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'e5dc1429-5dd0-4b6d-8f5d-c5fd82cad976', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1BA008DFF90 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'52dfb910-843c-4696-bd99-c7a6dcc98300', N'879b6128-dfb8-4e6c-8b68-09cc2e7dbdc9', CAST(0x0000A18500A85D90 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'552a6f93-db61-4845-92d2-c8d80ad75eb3', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C400D79B50 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'9188858b-88fb-46f5-92dc-c9bb06de47f7', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1BA00D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'a45ac63b-3546-4fd1-b3dc-cac7802a8cb1', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B700D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'19e53fcd-3f05-491e-947e-cb92f563f4df', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C600D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'1e1b8d93-6e61-4265-be50-cba590403df6', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B0008FA570 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'f9b834c0-ac7e-4a8d-8c7f-cbd8dcca6fc4', N'bba76e0c-1a15-4acd-886e-958b276174a7', CAST(0x0000A18500C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'de5439e2-6ac7-4e03-827f-cc3f239356e2', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1BE008462A0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'2c4f0a7a-bf26-4ad3-beac-cc5c336472c9', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C6013A3580 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'3acfe9d8-fb39-475f-bca0-ccd328f942d8', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17A01476480 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'a488041c-19ea-421d-9dfd-cd21c48fddaf', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18E00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'b339bb82-9840-47ff-9c29-cd711520d6a8', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C600C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'af2f39ec-e234-481b-9d1f-ce94cffcce45', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1A900876810 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'45e9f8d1-f7cf-490d-af6e-ce98ec273dfa', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A19B00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'6cf265c2-d9dc-45f9-af39-ce9c84d19088', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A18900C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'69c68e8a-d5d9-4ca4-b248-cf1280cbdd59', N'879b6128-dfb8-4e6c-8b68-09cc2e7dbdc9', CAST(0x0000A18500D94130 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'02084a39-0ce2-46bf-a056-d0415244919e', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C700C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'03a353ad-7be8-4a97-9fd0-d1008027b6eb', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C700C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'13124c9e-d9ce-48a1-be0e-d2204b4334dc', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18100C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'20c8d820-db76-4b21-93d3-d249e4ab3706', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C4013268F9 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'464179eb-82cd-4a8f-a22d-d2561d04247e', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17F008DB940 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'f1dd55cb-4333-415a-99fb-d2910d7c4770', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1AF00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'5bc8b16f-3b3c-4712-a2f4-d310f6d2956a', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A174011826C0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'26b599ed-5ea6-4c6f-8084-d352d0da7473', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1810098E948 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'3ffcf3c9-2dcf-4a3e-ac76-d43c4031e459', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1B800926490 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'5a9a7582-d5e7-406e-aead-d5521eb2d263', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17900A0ACD0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'659a51ea-e7c9-4e86-93f6-d59c457f1350', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A1890089E261 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'3e925d48-9e20-4e1a-b8a6-d5bb36421b64', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1BD008C59B0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'699daa87-c93d-452e-95d5-d5c0bcc5c163', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A18800DD972A AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'4cf07377-d66c-43fe-b74e-d6701ee948e2', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1AA00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'edd34f2f-2139-4c61-b2ae-d6e8c1c8fe48', N'6fd6ae6d-4753-4f00-a392-a4d4aadcd154', CAST(0x0000A18500907860 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'48cc252f-d18b-41fd-8d99-d78fddc0152f', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1AF00914B50 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'dfac7f36-014a-434a-a4fe-d8d3d86dbaef', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18000D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'f9aef6b1-793e-46d5-9cac-d955b28e1798', N'b200e933-d80a-415c-95bb-d42564e415a7', CAST(0x0000A185008ED280 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'4851398d-1d20-4afb-8502-d9e5a4eb1fb8', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A189011AE5E0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'aaf97aa7-4dda-4c36-b7d3-da3f057c865a', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18900C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'213eb46c-34a0-41a0-9e08-dabdc0a2ce8e', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A187009191A0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0c795e43-01a2-47a6-bc00-db39e587876f', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1B800970FE0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'f9db6046-72b1-4d60-8d3c-db87fa541a12', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17A012555C0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'11b379c3-c03a-4b32-a862-dcc7e4c962df', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1960144A560 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'a281ade6-fe44-4c5a-a365-ddaff26df67e', N'b182901f-aef9-4116-bf3d-abb6a03b5d5b', CAST(0x0000A18500DBBA00 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'b725c969-a050-46a7-8d78-ddc3ca4e5a5f', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17E01339E00 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'7dac5e8b-4af7-46df-9c25-def2fc7a6792', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17E011BB8D0 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'ff951205-e59a-4d0f-ac1e-df6c222de0bd', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C400D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'cb9be9f6-deb6-4007-bcde-e0f85e84c562', N'879b6128-dfb8-4e6c-8b68-09cc2e7dbdc9', CAST(0x0000A18500C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'7f69e790-5725-4bcd-ad89-e2073083dc2b', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A19700C41B20 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'a9a634ef-5dca-4421-b0c7-e4b6f88a3f0d', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C500923AE4 AS DateTime), 1, NULL)
GO
print 'Processed 400 total records'
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'50a45477-3249-40c5-b135-e547fb9956b3', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A194008E7B4B AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'e1ae9208-b21c-42d1-b914-e55056f7266f', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A186008A6D80 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'958d3027-16bb-41a1-bb0d-e5b1676b07b2', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17E008AB3D0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'05a33b1c-9793-4338-bf57-e6981a1ed375', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18000DA5A70 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'bf3e86a3-b3b9-4e1b-bbd9-e70711fec59a', N'd8d5817c-4a21-42bf-abca-f8521e3cd1d0', CAST(0x0000A18600D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'adf06ac3-f752-4878-b757-e7aee62be183', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A177008DB940 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0d528d2c-e5ee-496b-8f91-e7e45d695b37', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A19400AA49C0 AS DateTime), 0, N'fui al medico')
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'ce06a8d2-90fa-4964-ba78-e7f2f6a76e62', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1A400857BE0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'acf12fc4-1edf-4c71-94bb-e893dc4057d3', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18E012B1830 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'dcf7a5ed-1bdc-48e8-b546-e8c96b2de6af', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18500D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'69c7aa97-de69-40aa-9895-e8dd554bb6ae', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1A100830310 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'5ca7a406-7bbc-44fa-aec8-e8e9d3d46cca', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1BE00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'c90f59d9-d94b-47b8-bb22-e8f1df850db4', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C100D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'ad3c09a1-93af-45aa-ad5e-e90f4058d840', N'bba76e0c-1a15-4acd-886e-958b276174a7', CAST(0x0000A18600C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'145c229f-3fd9-4153-8c40-e943359dc7d5', N'2575858b-f8e8-4bbf-864e-88bc2354ed52', CAST(0x0000A185008ED280 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'35cf8fea-6154-44fb-a712-e95b74d83b64', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18C00D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'b7fecb04-0561-4a66-b1df-ea63ea42d372', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A1880092FFD8 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'c4369fc9-2341-48c1-adb9-eb07c0064139', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17900C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'1b7806e5-21af-4aba-a9c6-eb378abb879c', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17A00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'59e0eebf-ce26-4909-ae67-eb3b0da99c8e', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A19400DF4C10 AS DateTime), 1, N'vine del medico')
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'6861102b-6014-424e-84b4-eb9fc0f7704f', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1BF0137B7A1 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'868c7e55-2e3b-4ca4-ac3d-ec1b0b2e6617', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C101386806 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'35715342-c15b-4eee-b4cc-ec2cd27e560d', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B900883B00 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'05215cac-e40e-4da4-807d-ec741c1df11e', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1AA00D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'14095daa-31c0-42db-83ea-ecf93a6608df', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A19700864ED0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'92b83b23-2618-4650-85e3-ed0635181e84', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17400C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'1ee1f542-ffbb-4abb-b7b8-edeb1d990db1', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17E009523B0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'9839281a-e206-45b0-b8ff-ee38dbc2bac0', N'ea3a8f9b-09d2-4429-92ba-b2cb0f39feb5', CAST(0x0000A186009AA1F0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'e72cb9ae-d686-4822-8167-ee72e074f7ab', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C700DBBA00 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'4a3f7faf-d5f7-490c-9c48-eeb5516b6ac3', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17800C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'7cbe2495-989b-4f7e-b9d4-eee0fedf9e21', N'ea3a8f9b-09d2-4429-92ba-b2cb0f39feb5', CAST(0x0000A18600A2408E AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'4f3883e0-f95c-4d36-959d-efb04ffbe1e2', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A19600A59E70 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'7d584ad9-64f6-4721-9714-efcb56b2aec7', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17E00DFD8B0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'2e8acfa3-dc63-4752-b2d9-f0851ad45dd4', N'bba76e0c-1a15-4acd-886e-958b276174a7', CAST(0x0000A18501339E00 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'8decea69-dce3-4119-96be-f13b0b71d37f', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A19C00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'080b609b-353a-464e-af50-f1f518dd8d54', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C500DBBA00 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'990c5c26-3119-4ca2-9c44-f28b4ae784af', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C40089E0E0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'8a72eeac-cf00-4594-bb21-f31f4a00de3e', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17A008E8C30 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'2cfdddcb-fa12-4ab6-bec0-f32ec4e77498', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A187012E28B4 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'1b781243-1738-4c19-a1da-f3dbdbf845b5', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1BD00D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'3a8b9616-7b24-40b9-a9cb-f41190b2d82b', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1AF00D63BC0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'73456bce-51ed-4767-80d1-f46b64668015', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1C50086DB70 AS DateTime), 1, N'dgdgdgdgagagasgGAgaGAGagAGAGgagagGAgaggAGASGSGDGGSDGDGDGD')
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'088b6478-bce5-489b-afcc-f55a4fadad59', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C100C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'316e531b-9e60-4205-a098-f63551229e1a', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A17E00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'0bb8c979-968f-4332-95b9-f662ebeb6a0d', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17A00C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'6caa1ea6-acbc-4faf-8345-f83753162eec', N'7fbd9a7e-c19d-4ee8-add9-f2446ee42730', CAST(0x0000A196012D5B67 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'37f29b4c-6d16-4a9f-a942-f8dca495dd50', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1BF008A2730 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'705778ff-f659-4390-87a9-fa853fdaffd9', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A17A00F31290 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'9001d408-7c2e-440a-959c-fb275f4800bc', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1C100AA49C0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'd78c67f7-50af-41b3-9ef5-fb770d6435f2', N'2fc65ef5-bd2f-40ef-9864-73bcd34c3a7c', CAST(0x0000A18500986F70 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'91ab178b-ce5a-41a5-977e-fc1a8270feb9', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A1B7009191A0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'956053fc-c7b6-449e-9169-fc1d798522eb', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1A800C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'fe4ddb6c-3a29-4833-86f3-fcaa3f559758', N'866fe705-6468-4df8-aeb6-9a757515e21c', CAST(0x0000A18100DF49C0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'f717a17f-d89f-4d80-a6bd-fdc55a2fb8fd', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1B200C5C100 AS DateTime), 0, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'37b78d72-4ac4-4cdb-9a28-fdfdd53ed71c', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A18D00944712 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'96cf1b31-0555-4049-a232-ff560aa4c1c7', N'95f624cd-ad78-47a7-a332-ad1712a2795f', CAST(0x0000A1AA0087F4B0 AS DateTime), 1, NULL)
INSERT [dbo].[OfficeTime] ([OfficeTimeId], [UserId], [ActionTime], [Enter], [Comment]) VALUES (N'9d23100e-1f86-4e3b-914d-ffebafae648d', N'd4e754fd-e279-49c2-be96-0a3b860564c2', CAST(0x0000A19300C5C100 AS DateTime), 0, NULL)
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_CreateRole]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Roles_CreateRole]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
        BEGIN TRANSACTION
        SET @TranStarted = 1
    END
    ELSE
        SET @TranStarted = 0

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF (EXISTS(SELECT RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = LOWER(@RoleName) AND ApplicationId = @ApplicationId))
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    INSERT INTO dbo.aspnet_Roles
                (ApplicationId, RoleName, LoweredRoleName)
         VALUES (@ApplicationId, @RoleName, LOWER(@RoleName))

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        COMMIT TRANSACTION
    END

    RETURN(0)

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
GO
/****** Object:  View [dbo].[vw_aspnet_Users]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_Users]
  AS SELECT [dbo].[aspnet_Users].[ApplicationId], [dbo].[aspnet_Users].[UserId], [dbo].[aspnet_Users].[UserName], [dbo].[aspnet_Users].[LoweredUserName], [dbo].[aspnet_Users].[MobileAlias], [dbo].[aspnet_Users].[IsAnonymous], [dbo].[aspnet_Users].[LastActivityDate]
  FROM [dbo].[aspnet_Users]
GO
/****** Object:  View [dbo].[vw_aspnet_Roles]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_Roles]
  AS SELECT [dbo].[aspnet_Roles].[ApplicationId], [dbo].[aspnet_Roles].[RoleId], [dbo].[aspnet_Roles].[RoleName], [dbo].[aspnet_Roles].[LoweredRoleName], [dbo].[aspnet_Roles].[Description]
  FROM [dbo].[aspnet_Roles]
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_Paths]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_WebPartState_Paths]
  AS SELECT [dbo].[aspnet_Paths].[ApplicationId], [dbo].[aspnet_Paths].[PathId], [dbo].[aspnet_Paths].[Path], [dbo].[aspnet_Paths].[LoweredPath]
  FROM [dbo].[aspnet_Paths]
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_User]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_WebPartState_User]
  AS SELECT [dbo].[aspnet_PersonalizationPerUser].[PathId], [dbo].[aspnet_PersonalizationPerUser].[UserId], [DataSize]=DATALENGTH([dbo].[aspnet_PersonalizationPerUser].[PageSettings]), [dbo].[aspnet_PersonalizationPerUser].[LastUpdatedDate]
  FROM [dbo].[aspnet_PersonalizationPerUser]
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_Shared]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_WebPartState_Shared]
  AS SELECT [dbo].[aspnet_PersonalizationAllUsers].[PathId], [DataSize]=DATALENGTH([dbo].[aspnet_PersonalizationAllUsers].[PageSettings]), [dbo].[aspnet_PersonalizationAllUsers].[LastUpdatedDate]
  FROM [dbo].[aspnet_PersonalizationAllUsers]
GO
/****** Object:  View [dbo].[vw_aspnet_UsersInRoles]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_UsersInRoles]
  AS SELECT [dbo].[aspnet_UsersInRoles].[UserId], [dbo].[aspnet_UsersInRoles].[RoleId]
  FROM [dbo].[aspnet_UsersInRoles]
GO
/****** Object:  View [dbo].[vw_aspnet_Profiles]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_Profiles]
  AS SELECT [dbo].[aspnet_Profile].[UserId], [dbo].[aspnet_Profile].[LastUpdatedDate],
      [DataSize]=  DATALENGTH([dbo].[aspnet_Profile].[PropertyNames])
                 + DATALENGTH([dbo].[aspnet_Profile].[PropertyValuesString])
                 + DATALENGTH([dbo].[aspnet_Profile].[PropertyValuesBinary])
  FROM [dbo].[aspnet_Profile]
GO
/****** Object:  View [dbo].[vw_aspnet_MembershipUsers]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_MembershipUsers]
  AS SELECT [dbo].[aspnet_Membership].[UserId],
            [dbo].[aspnet_Membership].[PasswordFormat],
            [dbo].[aspnet_Membership].[MobilePIN],
            [dbo].[aspnet_Membership].[Email],
            [dbo].[aspnet_Membership].[LoweredEmail],
            [dbo].[aspnet_Membership].[PasswordQuestion],
            [dbo].[aspnet_Membership].[PasswordAnswer],
            [dbo].[aspnet_Membership].[IsApproved],
            [dbo].[aspnet_Membership].[IsLockedOut],
            [dbo].[aspnet_Membership].[CreateDate],
            [dbo].[aspnet_Membership].[LastLoginDate],
            [dbo].[aspnet_Membership].[LastPasswordChangedDate],
            [dbo].[aspnet_Membership].[LastLockoutDate],
            [dbo].[aspnet_Membership].[FailedPasswordAttemptCount],
            [dbo].[aspnet_Membership].[FailedPasswordAttemptWindowStart],
            [dbo].[aspnet_Membership].[FailedPasswordAnswerAttemptCount],
            [dbo].[aspnet_Membership].[FailedPasswordAnswerAttemptWindowStart],
            [dbo].[aspnet_Membership].[Comment],
            [dbo].[aspnet_Users].[ApplicationId],
            [dbo].[aspnet_Users].[UserName],
            [dbo].[aspnet_Users].[MobileAlias],
            [dbo].[aspnet_Users].[IsAnonymous],
            [dbo].[aspnet_Users].[LastActivityDate]
  FROM [dbo].[aspnet_Membership] INNER JOIN [dbo].[aspnet_Users]
      ON [dbo].[aspnet_Membership].[UserId] = [dbo].[aspnet_Users].[UserId]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_DeleteInactiveProfiles]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Profile_DeleteInactiveProfiles]
    @ApplicationName        nvarchar(256),
    @ProfileAuthOptions     int,
    @InactiveSinceDate      datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
    BEGIN
        SELECT  0
        RETURN
    END

    DELETE
    FROM    dbo.aspnet_Profile
    WHERE   UserId IN
            (   SELECT  UserId
                FROM    dbo.aspnet_Users u
                WHERE   ApplicationId = @ApplicationId
                        AND (LastActivityDate <= @InactiveSinceDate)
                        AND (
                                (@ProfileAuthOptions = 2)
                             OR (@ProfileAuthOptions = 0 AND IsAnonymous = 1)
                             OR (@ProfileAuthOptions = 1 AND IsAnonymous = 0)
                            )
            )

    SELECT  @@ROWCOUNT
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]
	@ApplicationName  nvarchar(256),
	@UserNames		  nvarchar(4000),
	@RoleNames		  nvarchar(4000)
AS
BEGIN
	DECLARE @AppId uniqueidentifier
	SELECT  @AppId = NULL
	SELECT  @AppId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
	IF (@AppId IS NULL)
		RETURN(2)


	DECLARE @TranStarted   bit
	SET @TranStarted = 0

	IF( @@TRANCOUNT = 0 )
	BEGIN
		BEGIN TRANSACTION
		SET @TranStarted = 1
	END

	DECLARE @tbNames  table(Name nvarchar(256) NOT NULL PRIMARY KEY)
	DECLARE @tbRoles  table(RoleId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @tbUsers  table(UserId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @Num	  int
	DECLARE @Pos	  int
	DECLARE @NextPos  int
	DECLARE @Name	  nvarchar(256)
	DECLARE @CountAll int
	DECLARE @CountU	  int
	DECLARE @CountR	  int


	SET @Num = 0
	SET @Pos = 1
	WHILE(@Pos <= LEN(@RoleNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N',', @RoleNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@RoleNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@RoleNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbRoles
	  SELECT RoleId
	  FROM   dbo.aspnet_Roles ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredRoleName AND ar.ApplicationId = @AppId
	SELECT @CountR = @@ROWCOUNT

	IF (@CountR <> @Num)
	BEGIN
		SELECT TOP 1 N'', Name
		FROM   @tbNames
		WHERE  LOWER(Name) NOT IN (SELECT ar.LoweredRoleName FROM dbo.aspnet_Roles ar,  @tbRoles r WHERE r.RoleId = ar.RoleId)
		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(2)
	END


	DELETE FROM @tbNames WHERE 1=1
	SET @Num = 0
	SET @Pos = 1


	WHILE(@Pos <= LEN(@UserNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N',', @UserNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@UserNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@UserNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbUsers
	  SELECT UserId
	  FROM   dbo.aspnet_Users ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredUserName AND ar.ApplicationId = @AppId

	SELECT @CountU = @@ROWCOUNT
	IF (@CountU <> @Num)
	BEGIN
		SELECT TOP 1 Name, N''
		FROM   @tbNames
		WHERE  LOWER(Name) NOT IN (SELECT au.LoweredUserName FROM dbo.aspnet_Users au,  @tbUsers u WHERE u.UserId = au.UserId)

		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(1)
	END

	SELECT  @CountAll = COUNT(*)
	FROM	dbo.aspnet_UsersInRoles ur, @tbUsers u, @tbRoles r
	WHERE   ur.UserId = u.UserId AND ur.RoleId = r.RoleId

	IF (@CountAll <> @CountU * @CountR)
	BEGIN
		SELECT TOP 1 UserName, RoleName
		FROM		 @tbUsers tu, @tbRoles tr, dbo.aspnet_Users u, dbo.aspnet_Roles r
		WHERE		 u.UserId = tu.UserId AND r.RoleId = tr.RoleId AND
					 tu.UserId NOT IN (SELECT ur.UserId FROM dbo.aspnet_UsersInRoles ur WHERE ur.RoleId = tr.RoleId) AND
					 tr.RoleId NOT IN (SELECT ur.RoleId FROM dbo.aspnet_UsersInRoles ur WHERE ur.UserId = tu.UserId)
		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(3)
	END

	DELETE FROM dbo.aspnet_UsersInRoles
	WHERE UserId IN (SELECT UserId FROM @tbUsers)
	  AND RoleId IN (SELECT RoleId FROM @tbRoles)
	IF( @TranStarted = 1 )
		COMMIT TRANSACTION
	RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_IsUserInRole]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_IsUserInRole]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(2)
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    DECLARE @RoleId uniqueidentifier
    SELECT  @RoleId = NULL

    SELECT  @UserId = UserId
    FROM    dbo.aspnet_Users
    WHERE   LoweredUserName = LOWER(@UserName) AND ApplicationId = @ApplicationId

    IF (@UserId IS NULL)
        RETURN(2)

    SELECT  @RoleId = RoleId
    FROM    dbo.aspnet_Roles
    WHERE   LoweredRoleName = LOWER(@RoleName) AND ApplicationId = @ApplicationId

    IF (@RoleId IS NULL)
        RETURN(3)

    IF (EXISTS( SELECT * FROM dbo.aspnet_UsersInRoles WHERE  UserId = @UserId AND RoleId = @RoleId))
        RETURN(1)
    ELSE
        RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_GetUsersInRoles]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_GetUsersInRoles]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)
     DECLARE @RoleId uniqueidentifier
     SELECT  @RoleId = NULL

     SELECT  @RoleId = RoleId
     FROM    dbo.aspnet_Roles
     WHERE   LOWER(@RoleName) = LoweredRoleName AND ApplicationId = @ApplicationId

     IF (@RoleId IS NULL)
         RETURN(1)

    SELECT u.UserName
    FROM   dbo.aspnet_Users u, dbo.aspnet_UsersInRoles ur
    WHERE  u.UserId = ur.UserId AND @RoleId = ur.RoleId AND u.ApplicationId = @ApplicationId
    ORDER BY u.UserName
    RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_GetRolesForUser]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_GetRolesForUser]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL

    SELECT  @UserId = UserId
    FROM    dbo.aspnet_Users
    WHERE   LoweredUserName = LOWER(@UserName) AND ApplicationId = @ApplicationId

    IF (@UserId IS NULL)
        RETURN(1)

    SELECT r.RoleName
    FROM   dbo.aspnet_Roles r, dbo.aspnet_UsersInRoles ur
    WHERE  r.RoleId = ur.RoleId AND r.ApplicationId = @ApplicationId AND ur.UserId = @UserId
    ORDER BY r.RoleName
    RETURN (0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_FindUsersInRole]    Script Date: 05/23/2013 15:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_FindUsersInRole]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256),
    @UserNameToMatch  nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)
     DECLARE @RoleId uniqueidentifier
     SELECT  @RoleId = NULL

     SELECT  @RoleId = RoleId
     FROM    dbo.aspnet_Roles
     WHERE   LOWER(@RoleName) = LoweredRoleName AND ApplicationId = @ApplicationId

     IF (@RoleId IS NULL)
         RETURN(1)

    SELECT u.UserName
    FROM   dbo.aspnet_Users u, dbo.aspnet_UsersInRoles ur
    WHERE  u.UserId = ur.UserId AND @RoleId = ur.RoleId AND u.ApplicationId = @ApplicationId AND LoweredUserName LIKE LOWER(@UserNameToMatch)
    ORDER BY u.UserName
    RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_AddUsersToRoles]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_AddUsersToRoles]
	@ApplicationName  nvarchar(256),
	@UserNames		  nvarchar(4000),
	@RoleNames		  nvarchar(4000),
	@CurrentTimeUtc   datetime
AS
BEGIN
	DECLARE @AppId uniqueidentifier
	SELECT  @AppId = NULL
	SELECT  @AppId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
	IF (@AppId IS NULL)
		RETURN(2)
	DECLARE @TranStarted   bit
	SET @TranStarted = 0

	IF( @@TRANCOUNT = 0 )
	BEGIN
		BEGIN TRANSACTION
		SET @TranStarted = 1
	END

	DECLARE @tbNames	table(Name nvarchar(256) NOT NULL PRIMARY KEY)
	DECLARE @tbRoles	table(RoleId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @tbUsers	table(UserId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @Num		int
	DECLARE @Pos		int
	DECLARE @NextPos	int
	DECLARE @Name		nvarchar(256)

	SET @Num = 0
	SET @Pos = 1
	WHILE(@Pos <= LEN(@RoleNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N',', @RoleNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@RoleNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@RoleNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbRoles
	  SELECT RoleId
	  FROM   dbo.aspnet_Roles ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredRoleName AND ar.ApplicationId = @AppId

	IF (@@ROWCOUNT <> @Num)
	BEGIN
		SELECT TOP 1 Name
		FROM   @tbNames
		WHERE  LOWER(Name) NOT IN (SELECT ar.LoweredRoleName FROM dbo.aspnet_Roles ar,  @tbRoles r WHERE r.RoleId = ar.RoleId)
		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(2)
	END

	DELETE FROM @tbNames WHERE 1=1
	SET @Num = 0
	SET @Pos = 1

	WHILE(@Pos <= LEN(@UserNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N',', @UserNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@UserNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@UserNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbUsers
	  SELECT UserId
	  FROM   dbo.aspnet_Users ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredUserName AND ar.ApplicationId = @AppId

	IF (@@ROWCOUNT <> @Num)
	BEGIN
		DELETE FROM @tbNames
		WHERE LOWER(Name) IN (SELECT LoweredUserName FROM dbo.aspnet_Users au,  @tbUsers u WHERE au.UserId = u.UserId)

		INSERT dbo.aspnet_Users (ApplicationId, UserId, UserName, LoweredUserName, IsAnonymous, LastActivityDate)
		  SELECT @AppId, NEWID(), Name, LOWER(Name), 0, @CurrentTimeUtc
		  FROM   @tbNames

		INSERT INTO @tbUsers
		  SELECT  UserId
		  FROM	dbo.aspnet_Users au, @tbNames t
		  WHERE   LOWER(t.Name) = au.LoweredUserName AND au.ApplicationId = @AppId
	END

	IF (EXISTS (SELECT * FROM dbo.aspnet_UsersInRoles ur, @tbUsers tu, @tbRoles tr WHERE tu.UserId = ur.UserId AND tr.RoleId = ur.RoleId))
	BEGIN
		SELECT TOP 1 UserName, RoleName
		FROM		 dbo.aspnet_UsersInRoles ur, @tbUsers tu, @tbRoles tr, aspnet_Users u, aspnet_Roles r
		WHERE		u.UserId = tu.UserId AND r.RoleId = tr.RoleId AND tu.UserId = ur.UserId AND tr.RoleId = ur.RoleId

		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(3)
	END

	INSERT INTO dbo.aspnet_UsersInRoles (UserId, RoleId)
	SELECT UserId, RoleId
	FROM @tbUsers, @tbRoles

	IF( @TranStarted = 1 )
		COMMIT TRANSACTION
	RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Users_DeleteUser]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Users_DeleteUser]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @TablesToDeleteFrom int,
    @NumTablesDeletedFrom int OUTPUT
AS
BEGIN
    DECLARE @UserId               uniqueidentifier
    SELECT  @UserId               = NULL
    SELECT  @NumTablesDeletedFrom = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
	SET @TranStarted = 0

    DECLARE @ErrorCode   int
    DECLARE @RowCount    int

    SET @ErrorCode = 0
    SET @RowCount  = 0

    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a
    WHERE   u.LoweredUserName       = LOWER(@UserName)
        AND u.ApplicationId         = a.ApplicationId
        AND LOWER(@ApplicationName) = a.LoweredApplicationName

    IF (@UserId IS NULL)
    BEGIN
        GOTO Cleanup
    END

    -- Delete from Membership table if (@TablesToDeleteFrom & 1) is set
    IF ((@TablesToDeleteFrom & 1) <> 0 AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_MembershipUsers') AND (type = 'V'))))
    BEGIN
        DELETE FROM dbo.aspnet_Membership WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
               @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_UsersInRoles table if (@TablesToDeleteFrom & 2) is set
    IF ((@TablesToDeleteFrom & 2) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_UsersInRoles') AND (type = 'V'))) )
    BEGIN
        DELETE FROM dbo.aspnet_UsersInRoles WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_Profile table if (@TablesToDeleteFrom & 4) is set
    IF ((@TablesToDeleteFrom & 4) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_Profiles') AND (type = 'V'))) )
    BEGIN
        DELETE FROM dbo.aspnet_Profile WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_PersonalizationPerUser table if (@TablesToDeleteFrom & 8) is set
    IF ((@TablesToDeleteFrom & 8) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_WebPartState_User') AND (type = 'V'))) )
    BEGIN
        DELETE FROM dbo.aspnet_PersonalizationPerUser WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_Users table if (@TablesToDeleteFrom & 1,2,4 & 8) are all set
    IF ((@TablesToDeleteFrom & 1) <> 0 AND
        (@TablesToDeleteFrom & 2) <> 0 AND
        (@TablesToDeleteFrom & 4) <> 0 AND
        (@TablesToDeleteFrom & 8) <> 0 AND
        (EXISTS (SELECT UserId FROM dbo.aspnet_Users WHERE @UserId = UserId)))
    BEGIN
        DELETE FROM dbo.aspnet_Users WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    IF( @TranStarted = 1 )
    BEGIN
	    SET @TranStarted = 0
	    COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:
    SET @NumTablesDeletedFrom = 0

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
	    ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_DeleteRole]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Roles_DeleteRole]
    @ApplicationName            nvarchar(256),
    @RoleName                   nvarchar(256),
    @DeleteOnlyIfRoleIsEmpty    bit
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
        BEGIN TRANSACTION
        SET @TranStarted = 1
    END
    ELSE
        SET @TranStarted = 0

    DECLARE @RoleId   uniqueidentifier
    SELECT  @RoleId = NULL
    SELECT  @RoleId = RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = LOWER(@RoleName) AND ApplicationId = @ApplicationId

    IF (@RoleId IS NULL)
    BEGIN
        SELECT @ErrorCode = 1
        GOTO Cleanup
    END
    IF (@DeleteOnlyIfRoleIsEmpty <> 0)
    BEGIN
        IF (EXISTS (SELECT RoleId FROM dbo.aspnet_UsersInRoles  WHERE @RoleId = RoleId))
        BEGIN
            SELECT @ErrorCode = 2
            GOTO Cleanup
        END
    END


    DELETE FROM dbo.aspnet_UsersInRoles  WHERE @RoleId = RoleId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    DELETE FROM dbo.aspnet_Roles WHERE @RoleId = RoleId  AND ApplicationId = @ApplicationId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        COMMIT TRANSACTION
    END

    RETURN(0)

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UpdateUserInfo]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_UpdateUserInfo]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @IsPasswordCorrect              bit,
    @UpdateLastLoginActivityDate    bit,
    @MaxInvalidPasswordAttempts     int,
    @PasswordAttemptWindow          int,
    @CurrentTimeUtc                 datetime,
    @LastLoginDate                  datetime,
    @LastActivityDate               datetime
AS
BEGIN
    DECLARE @UserId                                 uniqueidentifier
    DECLARE @IsApproved                             bit
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId,
            @IsApproved = m.IsApproved,
            @IsLockedOut = m.IsLockedOut,
            @LastLockoutDate = m.LastLockoutDate,
            @FailedPasswordAttemptCount = m.FailedPasswordAttemptCount,
            @FailedPasswordAttemptWindowStart = m.FailedPasswordAttemptWindowStart,
            @FailedPasswordAnswerAttemptCount = m.FailedPasswordAnswerAttemptCount,
            @FailedPasswordAnswerAttemptWindowStart = m.FailedPasswordAnswerAttemptWindowStart
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m WITH ( UPDLOCK )
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF ( @@rowcount = 0 )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    IF( @IsLockedOut = 1 )
    BEGIN
        GOTO Cleanup
    END

    IF( @IsPasswordCorrect = 0 )
    BEGIN
        IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAttemptWindowStart ) )
        BEGIN
            SET @FailedPasswordAttemptWindowStart = @CurrentTimeUtc
            SET @FailedPasswordAttemptCount = 1
        END
        ELSE
        BEGIN
            SET @FailedPasswordAttemptWindowStart = @CurrentTimeUtc
            SET @FailedPasswordAttemptCount = @FailedPasswordAttemptCount + 1
        END

        BEGIN
            IF( @FailedPasswordAttemptCount >= @MaxInvalidPasswordAttempts )
            BEGIN
                SET @IsLockedOut = 1
                SET @LastLockoutDate = @CurrentTimeUtc
            END
        END
    END
    ELSE
    BEGIN
        IF( @FailedPasswordAttemptCount > 0 OR @FailedPasswordAnswerAttemptCount > 0 )
        BEGIN
            SET @FailedPasswordAttemptCount = 0
            SET @FailedPasswordAttemptWindowStart = CONVERT( datetime, '17540101', 112 )
            SET @FailedPasswordAnswerAttemptCount = 0
            SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, '17540101', 112 )
            SET @LastLockoutDate = CONVERT( datetime, '17540101', 112 )
        END
    END

    IF( @UpdateLastLoginActivityDate = 1 )
    BEGIN
        UPDATE  dbo.aspnet_Users
        SET     LastActivityDate = @LastActivityDate
        WHERE   @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END

        UPDATE  dbo.aspnet_Membership
        SET     LastLoginDate = @LastLoginDate
        WHERE   UserId = @UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END


    UPDATE dbo.aspnet_Membership
    SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
        FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
        FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
        FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
        FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
    WHERE @UserId = UserId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UpdateUser]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_UpdateUser]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @Email                nvarchar(256),
    @Comment              ntext,
    @IsApproved           bit,
    @LastLoginDate        datetime,
    @LastActivityDate     datetime,
    @UniqueEmail          int,
    @CurrentTimeUtc       datetime
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId, @ApplicationId = a.ApplicationId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF (@UserId IS NULL)
        RETURN(1)

    IF (@UniqueEmail = 1)
    BEGIN
        IF (EXISTS (SELECT *
                    FROM  dbo.aspnet_Membership WITH (UPDLOCK, HOLDLOCK)
                    WHERE ApplicationId = @ApplicationId  AND @UserId <> UserId AND LoweredEmail = LOWER(@Email)))
        BEGIN
            RETURN(7)
        END
    END

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
	SET @TranStarted = 0

    UPDATE dbo.aspnet_Users WITH (ROWLOCK)
    SET
         LastActivityDate = @LastActivityDate
    WHERE
       @UserId = UserId

    IF( @@ERROR <> 0 )
        GOTO Cleanup

    UPDATE dbo.aspnet_Membership WITH (ROWLOCK)
    SET
         Email            = @Email,
         LoweredEmail     = LOWER(@Email),
         Comment          = @Comment,
         IsApproved       = @IsApproved,
         LastLoginDate    = @LastLoginDate
    WHERE
       @UserId = UserId

    IF( @@ERROR <> 0 )
        GOTO Cleanup

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN -1
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UnlockUser]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_UnlockUser]
    @ApplicationName                         nvarchar(256),
    @UserName                                nvarchar(256)
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF ( @UserId IS NULL )
        RETURN 1

    UPDATE dbo.aspnet_Membership
    SET IsLockedOut = 0,
        FailedPasswordAttemptCount = 0,
        FailedPasswordAttemptWindowStart = CONVERT( datetime, '17540101', 112 ),
        FailedPasswordAnswerAttemptCount = 0,
        FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, '17540101', 112 ),
        LastLockoutDate = CONVERT( datetime, '17540101', 112 )
    WHERE @UserId = UserId

    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_SetPassword]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_SetPassword]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @NewPassword      nvarchar(128),
    @PasswordSalt     nvarchar(128),
    @CurrentTimeUtc   datetime,
    @PasswordFormat   int = 0
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF (@UserId IS NULL)
        RETURN(1)

    UPDATE dbo.aspnet_Membership
    SET Password = @NewPassword, PasswordFormat = @PasswordFormat, PasswordSalt = @PasswordSalt,
        LastPasswordChangedDate = @CurrentTimeUtc
    WHERE @UserId = UserId
    RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_ResetPassword]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_ResetPassword]
    @ApplicationName             nvarchar(256),
    @UserName                    nvarchar(256),
    @NewPassword                 nvarchar(128),
    @MaxInvalidPasswordAttempts  int,
    @PasswordAttemptWindow       int,
    @PasswordSalt                nvarchar(128),
    @CurrentTimeUtc              datetime,
    @PasswordFormat              int = 0,
    @PasswordAnswer              nvarchar(128) = NULL
AS
BEGIN
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @UserId                                 uniqueidentifier
    SET     @UserId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF ( @UserId IS NULL )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    SELECT @IsLockedOut = IsLockedOut,
           @LastLockoutDate = LastLockoutDate,
           @FailedPasswordAttemptCount = FailedPasswordAttemptCount,
           @FailedPasswordAttemptWindowStart = FailedPasswordAttemptWindowStart,
           @FailedPasswordAnswerAttemptCount = FailedPasswordAnswerAttemptCount,
           @FailedPasswordAnswerAttemptWindowStart = FailedPasswordAnswerAttemptWindowStart
    FROM dbo.aspnet_Membership WITH ( UPDLOCK )
    WHERE @UserId = UserId

    IF( @IsLockedOut = 1 )
    BEGIN
        SET @ErrorCode = 99
        GOTO Cleanup
    END

    UPDATE dbo.aspnet_Membership
    SET    Password = @NewPassword,
           LastPasswordChangedDate = @CurrentTimeUtc,
           PasswordFormat = @PasswordFormat,
           PasswordSalt = @PasswordSalt
    WHERE  @UserId = UserId AND
           ( ( @PasswordAnswer IS NULL ) OR ( LOWER( PasswordAnswer ) = LOWER( @PasswordAnswer ) ) )

    IF ( @@ROWCOUNT = 0 )
        BEGIN
            IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAnswerAttemptWindowStart ) )
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = 1
            END
            ELSE
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount + 1
            END

            BEGIN
                IF( @FailedPasswordAnswerAttemptCount >= @MaxInvalidPasswordAttempts )
                BEGIN
                    SET @IsLockedOut = 1
                    SET @LastLockoutDate = @CurrentTimeUtc
                END
            END

            SET @ErrorCode = 3
        END
    ELSE
        BEGIN
            IF( @FailedPasswordAnswerAttemptCount > 0 )
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = 0
                SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, '17540101', 112 )
            END
        END

    IF( NOT ( @PasswordAnswer IS NULL ) )
    BEGIN
        UPDATE dbo.aspnet_Membership
        SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
            FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
            FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
            FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
            FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
        WHERE @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByUserId]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByUserId]
    @UserId               uniqueidentifier,
    @CurrentTimeUtc       datetime,
    @UpdateLastActivity   bit = 0
AS
BEGIN
    IF ( @UpdateLastActivity = 1 )
    BEGIN
        UPDATE   dbo.aspnet_Users
        SET      LastActivityDate = @CurrentTimeUtc
        FROM     dbo.aspnet_Users
        WHERE    @UserId = UserId

        IF ( @@ROWCOUNT = 0 ) -- User ID not found
            RETURN -1
    END

    SELECT  m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate, m.LastLoginDate, u.LastActivityDate,
            m.LastPasswordChangedDate, u.UserName, m.IsLockedOut,
            m.LastLockoutDate
    FROM    dbo.aspnet_Users u, dbo.aspnet_Membership m
    WHERE   @UserId = u.UserId AND u.UserId = m.UserId

    IF ( @@ROWCOUNT = 0 ) -- User ID not found
       RETURN -1

    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByName]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByName]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @CurrentTimeUtc       datetime,
    @UpdateLastActivity   bit = 0
AS
BEGIN
    DECLARE @UserId uniqueidentifier

    IF (@UpdateLastActivity = 1)
    BEGIN
        -- select user ID from aspnet_users table
        SELECT TOP 1 @UserId = u.UserId
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE    LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                LOWER(@UserName) = u.LoweredUserName AND u.UserId = m.UserId

        IF (@@ROWCOUNT = 0) -- Username not found
            RETURN -1

        UPDATE   dbo.aspnet_Users
        SET      LastActivityDate = @CurrentTimeUtc
        WHERE    @UserId = UserId

        SELECT m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
                m.CreateDate, m.LastLoginDate, u.LastActivityDate, m.LastPasswordChangedDate,
                u.UserId, m.IsLockedOut, m.LastLockoutDate
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE  @UserId = u.UserId AND u.UserId = m.UserId 
    END
    ELSE
    BEGIN
        SELECT TOP 1 m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
                m.CreateDate, m.LastLoginDate, u.LastActivityDate, m.LastPasswordChangedDate,
                u.UserId, m.IsLockedOut,m.LastLockoutDate
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE    LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                LOWER(@UserName) = u.LoweredUserName AND u.UserId = m.UserId

        IF (@@ROWCOUNT = 0) -- Username not found
            RETURN -1
    END

    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByEmail]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByEmail]
    @ApplicationName  nvarchar(256),
    @Email            nvarchar(256)
AS
BEGIN
    IF( @Email IS NULL )
        SELECT  u.UserName
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                u.UserId = m.UserId AND
                m.LoweredEmail IS NULL
    ELSE
        SELECT  u.UserName
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                u.UserId = m.UserId AND
                LOWER(@Email) = m.LoweredEmail

    IF (@@rowcount = 0)
        RETURN(1)
    RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetPasswordWithFormat]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_GetPasswordWithFormat]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @UpdateLastLoginActivityDate    bit,
    @CurrentTimeUtc                 datetime
AS
BEGIN
    DECLARE @IsLockedOut                        bit
    DECLARE @UserId                             uniqueidentifier
    DECLARE @Password                           nvarchar(128)
    DECLARE @PasswordSalt                       nvarchar(128)
    DECLARE @PasswordFormat                     int
    DECLARE @FailedPasswordAttemptCount         int
    DECLARE @FailedPasswordAnswerAttemptCount   int
    DECLARE @IsApproved                         bit
    DECLARE @LastActivityDate                   datetime
    DECLARE @LastLoginDate                      datetime

    SELECT  @UserId          = NULL

    SELECT  @UserId = u.UserId, @IsLockedOut = m.IsLockedOut, @Password=Password, @PasswordFormat=PasswordFormat,
            @PasswordSalt=PasswordSalt, @FailedPasswordAttemptCount=FailedPasswordAttemptCount,
		    @FailedPasswordAnswerAttemptCount=FailedPasswordAnswerAttemptCount, @IsApproved=IsApproved,
            @LastActivityDate = LastActivityDate, @LastLoginDate = LastLoginDate
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF (@UserId IS NULL)
        RETURN 1

    IF (@IsLockedOut = 1)
        RETURN 99

    SELECT   @Password, @PasswordFormat, @PasswordSalt, @FailedPasswordAttemptCount,
             @FailedPasswordAnswerAttemptCount, @IsApproved, @LastLoginDate, @LastActivityDate

    IF (@UpdateLastLoginActivityDate = 1 AND @IsApproved = 1)
    BEGIN
        UPDATE  dbo.aspnet_Membership
        SET     LastLoginDate = @CurrentTimeUtc
        WHERE   UserId = @UserId

        UPDATE  dbo.aspnet_Users
        SET     LastActivityDate = @CurrentTimeUtc
        WHERE   @UserId = UserId
    END


    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetPassword]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_GetPassword]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @MaxInvalidPasswordAttempts     int,
    @PasswordAttemptWindow          int,
    @CurrentTimeUtc                 datetime,
    @PasswordAnswer                 nvarchar(128) = NULL
AS
BEGIN
    DECLARE @UserId                                 uniqueidentifier
    DECLARE @PasswordFormat                         int
    DECLARE @Password                               nvarchar(128)
    DECLARE @passAns                                nvarchar(128)
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId,
            @Password = m.Password,
            @passAns = m.PasswordAnswer,
            @PasswordFormat = m.PasswordFormat,
            @IsLockedOut = m.IsLockedOut,
            @LastLockoutDate = m.LastLockoutDate,
            @FailedPasswordAttemptCount = m.FailedPasswordAttemptCount,
            @FailedPasswordAttemptWindowStart = m.FailedPasswordAttemptWindowStart,
            @FailedPasswordAnswerAttemptCount = m.FailedPasswordAnswerAttemptCount,
            @FailedPasswordAnswerAttemptWindowStart = m.FailedPasswordAnswerAttemptWindowStart
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m WITH ( UPDLOCK )
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF ( @@rowcount = 0 )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    IF( @IsLockedOut = 1 )
    BEGIN
        SET @ErrorCode = 99
        GOTO Cleanup
    END

    IF ( NOT( @PasswordAnswer IS NULL ) )
    BEGIN
        IF( ( @passAns IS NULL ) OR ( LOWER( @passAns ) <> LOWER( @PasswordAnswer ) ) )
        BEGIN
            IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAnswerAttemptWindowStart ) )
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = 1
            END
            ELSE
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount + 1
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
            END

            BEGIN
                IF( @FailedPasswordAnswerAttemptCount >= @MaxInvalidPasswordAttempts )
                BEGIN
                    SET @IsLockedOut = 1
                    SET @LastLockoutDate = @CurrentTimeUtc
                END
            END

            SET @ErrorCode = 3
        END
        ELSE
        BEGIN
            IF( @FailedPasswordAnswerAttemptCount > 0 )
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = 0
                SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, '17540101', 112 )
            END
        END

        UPDATE dbo.aspnet_Membership
        SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
            FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
            FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
            FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
            FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
        WHERE @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    IF( @ErrorCode = 0 )
        SELECT @Password, @PasswordFormat

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetNumberOfUsersOnline]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_GetNumberOfUsersOnline]
    @ApplicationName            nvarchar(256),
    @MinutesSinceLastInActive   int,
    @CurrentTimeUtc             datetime
AS
BEGIN
    DECLARE @DateActive datetime
    SELECT  @DateActive = DATEADD(minute,  -(@MinutesSinceLastInActive), @CurrentTimeUtc)

    DECLARE @NumOnline int
    SELECT  @NumOnline = COUNT(*)
    FROM    dbo.aspnet_Users u(NOLOCK),
            dbo.aspnet_Applications a(NOLOCK),
            dbo.aspnet_Membership m(NOLOCK)
    WHERE   u.ApplicationId = a.ApplicationId                  AND
            LastActivityDate > @DateActive                     AND
            a.LoweredApplicationName = LOWER(@ApplicationName) AND
            u.UserId = m.UserId
    RETURN(@NumOnline)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetAllUsers]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_GetAllUsers]
    @ApplicationName       nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0


    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
    SELECT u.UserId
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u
    WHERE  u.ApplicationId = @ApplicationId AND u.UserId = m.UserId
    ORDER BY u.UserName

    SELECT @TotalRecords = @@ROWCOUNT

    SELECT u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY u.UserName
    RETURN @TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_FindUsersByName]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_FindUsersByName]
    @ApplicationName       nvarchar(256),
    @UserNameToMatch       nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
        SELECT u.UserId
        FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND u.LoweredUserName LIKE LOWER(@UserNameToMatch)
        ORDER BY u.UserName


    SELECT  u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY u.UserName

    SELECT  @TotalRecords = COUNT(*)
    FROM    #PageIndexForUsers
    RETURN @TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_FindUsersByEmail]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_FindUsersByEmail]
    @ApplicationName       nvarchar(256),
    @EmailToMatch          nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    IF( @EmailToMatch IS NULL )
        INSERT INTO #PageIndexForUsers (UserId)
            SELECT u.UserId
            FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
            WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND m.Email IS NULL
            ORDER BY m.LoweredEmail
    ELSE
        INSERT INTO #PageIndexForUsers (UserId)
            SELECT u.UserId
            FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
            WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND m.LoweredEmail LIKE LOWER(@EmailToMatch)
            ORDER BY m.LoweredEmail

    SELECT  u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY m.LoweredEmail

    SELECT  @TotalRecords = COUNT(*)
    FROM    #PageIndexForUsers
    RETURN @TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_CreateUser]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_CreateUser]
    @ApplicationName                        nvarchar(256),
    @UserName                               nvarchar(256),
    @Password                               nvarchar(128),
    @PasswordSalt                           nvarchar(128),
    @Email                                  nvarchar(256),
    @PasswordQuestion                       nvarchar(256),
    @PasswordAnswer                         nvarchar(128),
    @IsApproved                             bit,
    @CurrentTimeUtc                         datetime,
    @CreateDate                             datetime = NULL,
    @UniqueEmail                            int      = 0,
    @PasswordFormat                         int      = 0,
    @UserId                                 uniqueidentifier OUTPUT
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @NewUserId uniqueidentifier
    SELECT @NewUserId = NULL

    DECLARE @IsLockedOut bit
    SET @IsLockedOut = 0

    DECLARE @LastLockoutDate  datetime
    SET @LastLockoutDate = CONVERT( datetime, '17540101', 112 )

    DECLARE @FailedPasswordAttemptCount int
    SET @FailedPasswordAttemptCount = 0

    DECLARE @FailedPasswordAttemptWindowStart  datetime
    SET @FailedPasswordAttemptWindowStart = CONVERT( datetime, '17540101', 112 )

    DECLARE @FailedPasswordAnswerAttemptCount int
    SET @FailedPasswordAnswerAttemptCount = 0

    DECLARE @FailedPasswordAnswerAttemptWindowStart  datetime
    SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, '17540101', 112 )

    DECLARE @NewUserCreated bit
    DECLARE @ReturnValue   int
    SET @ReturnValue = 0

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    SET @CreateDate = @CurrentTimeUtc

    SELECT  @NewUserId = UserId FROM dbo.aspnet_Users WHERE LOWER(@UserName) = LoweredUserName AND @ApplicationId = ApplicationId
    IF ( @NewUserId IS NULL )
    BEGIN
        SET @NewUserId = @UserId
        EXEC @ReturnValue = dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, 0, @CreateDate, @NewUserId OUTPUT
        SET @NewUserCreated = 1
    END
    ELSE
    BEGIN
        SET @NewUserCreated = 0
        IF( @NewUserId <> @UserId AND @UserId IS NOT NULL )
        BEGIN
            SET @ErrorCode = 6
            GOTO Cleanup
        END
    END

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @ReturnValue = -1 )
    BEGIN
        SET @ErrorCode = 10
        GOTO Cleanup
    END

    IF ( EXISTS ( SELECT UserId
                  FROM   dbo.aspnet_Membership
                  WHERE  @NewUserId = UserId ) )
    BEGIN
        SET @ErrorCode = 6
        GOTO Cleanup
    END

    SET @UserId = @NewUserId

    IF (@UniqueEmail = 1)
    BEGIN
        IF (EXISTS (SELECT *
                    FROM  dbo.aspnet_Membership m WITH ( UPDLOCK, HOLDLOCK )
                    WHERE ApplicationId = @ApplicationId AND LoweredEmail = LOWER(@Email)))
        BEGIN
            SET @ErrorCode = 7
            GOTO Cleanup
        END
    END

    IF (@NewUserCreated = 0)
    BEGIN
        UPDATE dbo.aspnet_Users
        SET    LastActivityDate = @CreateDate
        WHERE  @UserId = UserId
        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    INSERT INTO dbo.aspnet_Membership
                ( ApplicationId,
                  UserId,
                  Password,
                  PasswordSalt,
                  Email,
                  LoweredEmail,
                  PasswordQuestion,
                  PasswordAnswer,
                  PasswordFormat,
                  IsApproved,
                  IsLockedOut,
                  CreateDate,
                  LastLoginDate,
                  LastPasswordChangedDate,
                  LastLockoutDate,
                  FailedPasswordAttemptCount,
                  FailedPasswordAttemptWindowStart,
                  FailedPasswordAnswerAttemptCount,
                  FailedPasswordAnswerAttemptWindowStart )
         VALUES ( @ApplicationId,
                  @UserId,
                  @Password,
                  @PasswordSalt,
                  @Email,
                  LOWER(@Email),
                  @PasswordQuestion,
                  @PasswordAnswer,
                  @PasswordFormat,
                  @IsApproved,
                  @IsLockedOut,
                  @CreateDate,
                  @CreateDate,
                  @CreateDate,
                  @LastLockoutDate,
                  @FailedPasswordAttemptCount,
                  @FailedPasswordAttemptWindowStart,
                  @FailedPasswordAnswerAttemptCount,
                  @FailedPasswordAnswerAttemptWindowStart )

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
	    SET @TranStarted = 0
	    COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]
    @ApplicationName       nvarchar(256),
    @UserName              nvarchar(256),
    @NewPasswordQuestion   nvarchar(256),
    @NewPasswordAnswer     nvarchar(128)
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Membership m, dbo.aspnet_Users u, dbo.aspnet_Applications a
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId
    IF (@UserId IS NULL)
    BEGIN
        RETURN(1)
    END

    UPDATE dbo.aspnet_Membership
    SET    PasswordQuestion = @NewPasswordQuestion, PasswordAnswer = @NewPasswordAnswer
    WHERE  UserId=@UserId
    RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_AnyDataInTables]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_AnyDataInTables]
    @TablesToCheck int
AS
BEGIN
    -- Check Membership table if (@TablesToCheck & 1) is set
    IF ((@TablesToCheck & 1) <> 0 AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_MembershipUsers') AND (type = 'V'))))
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Membership))
        BEGIN
            SELECT N'aspnet_Membership'
            RETURN
        END
    END

    -- Check aspnet_Roles table if (@TablesToCheck & 2) is set
    IF ((@TablesToCheck & 2) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_Roles') AND (type = 'V'))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 RoleId FROM dbo.aspnet_Roles))
        BEGIN
            SELECT N'aspnet_Roles'
            RETURN
        END
    END

    -- Check aspnet_Profile table if (@TablesToCheck & 4) is set
    IF ((@TablesToCheck & 4) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_Profiles') AND (type = 'V'))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Profile))
        BEGIN
            SELECT N'aspnet_Profile'
            RETURN
        END
    END

    -- Check aspnet_PersonalizationPerUser table if (@TablesToCheck & 8) is set
    IF ((@TablesToCheck & 8) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_WebPartState_User') AND (type = 'V'))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_PersonalizationPerUser))
        BEGIN
            SELECT N'aspnet_PersonalizationPerUser'
            RETURN
        END
    END

    -- Check aspnet_PersonalizationPerUser table if (@TablesToCheck & 16) is set
    IF ((@TablesToCheck & 16) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'aspnet_WebEvent_LogEvent') AND (type = 'P'))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 * FROM dbo.aspnet_WebEvent_Events))
        BEGIN
            SELECT N'aspnet_WebEvent_Events'
            RETURN
        END
    END

    -- Check aspnet_Users table if (@TablesToCheck & 1,2,4 & 8) are all set
    IF ((@TablesToCheck & 1) <> 0 AND
        (@TablesToCheck & 2) <> 0 AND
        (@TablesToCheck & 4) <> 0 AND
        (@TablesToCheck & 8) <> 0 AND
        (@TablesToCheck & 32) <> 0 AND
        (@TablesToCheck & 128) <> 0 AND
        (@TablesToCheck & 256) <> 0 AND
        (@TablesToCheck & 512) <> 0 AND
        (@TablesToCheck & 1024) <> 0)
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Users))
        BEGIN
            SELECT N'aspnet_Users'
            RETURN
        END
        IF (EXISTS(SELECT TOP 1 ApplicationId FROM dbo.aspnet_Applications))
        BEGIN
            SELECT N'aspnet_Applications'
            RETURN
        END
    END
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_ResetUserState]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_ResetUserState] (
    @Count                  int                 OUT,
    @ApplicationName        NVARCHAR(256),
    @InactiveSinceDate      DATETIME            = NULL,
    @UserName               NVARCHAR(256)       = NULL,
    @Path                   NVARCHAR(256)       = NULL)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
    BEGIN
        DELETE FROM dbo.aspnet_PersonalizationPerUser
        WHERE Id IN (SELECT PerUser.Id
                     FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths
                     WHERE Paths.ApplicationId = @ApplicationId
                           AND PerUser.UserId = Users.UserId
                           AND PerUser.PathId = Paths.PathId
                           AND (@InactiveSinceDate IS NULL OR Users.LastActivityDate <= @InactiveSinceDate)
                           AND (@UserName IS NULL OR Users.LoweredUserName = LOWER(@UserName))
                           AND (@Path IS NULL OR Paths.LoweredPath = LOWER(@Path)))

        SELECT @Count = @@ROWCOUNT
    END
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_ResetSharedState]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_ResetSharedState] (
    @Count int OUT,
    @ApplicationName NVARCHAR(256),
    @Path NVARCHAR(256))
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
    BEGIN
        DELETE FROM dbo.aspnet_PersonalizationAllUsers
        WHERE PathId IN
            (SELECT AllUsers.PathId
             FROM dbo.aspnet_PersonalizationAllUsers AllUsers, dbo.aspnet_Paths Paths
             WHERE Paths.ApplicationId = @ApplicationId
                   AND AllUsers.PathId = Paths.PathId
                   AND Paths.LoweredPath = LOWER(@Path))

        SELECT @Count = @@ROWCOUNT
    END
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_GetCountOfState]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_GetCountOfState] (
    @Count int OUT,
    @AllUsersScope bit,
    @ApplicationName NVARCHAR(256),
    @Path NVARCHAR(256) = NULL,
    @UserName NVARCHAR(256) = NULL,
    @InactiveSinceDate DATETIME = NULL)
AS
BEGIN

    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
        IF (@AllUsersScope = 1)
            SELECT @Count = COUNT(*)
            FROM dbo.aspnet_PersonalizationAllUsers AllUsers, dbo.aspnet_Paths Paths
            WHERE Paths.ApplicationId = @ApplicationId
                  AND AllUsers.PathId = Paths.PathId
                  AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
        ELSE
            SELECT @Count = COUNT(*)
            FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths
            WHERE Paths.ApplicationId = @ApplicationId
                  AND PerUser.UserId = Users.UserId
                  AND PerUser.PathId = Paths.PathId
                  AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
                  AND (@UserName IS NULL OR Users.LoweredUserName LIKE LOWER(@UserName))
                  AND (@InactiveSinceDate IS NULL OR Users.LastActivityDate <= @InactiveSinceDate)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_FindState]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_FindState] (
    @AllUsersScope bit,
    @ApplicationName NVARCHAR(256),
    @PageIndex              INT,
    @PageSize               INT,
    @Path NVARCHAR(256) = NULL,
    @UserName NVARCHAR(256) = NULL,
    @InactiveSinceDate DATETIME = NULL)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        RETURN

    -- Set the page bounds
    DECLARE @PageLowerBound INT
    DECLARE @PageUpperBound INT
    DECLARE @TotalRecords   INT
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table to store the selected results
    CREATE TABLE #PageIndex (
        IndexId int IDENTITY (0, 1) NOT NULL,
        ItemId UNIQUEIDENTIFIER
    )

    IF (@AllUsersScope = 1)
    BEGIN
        -- Insert into our temp table
        INSERT INTO #PageIndex (ItemId)
        SELECT Paths.PathId
        FROM dbo.aspnet_Paths Paths,
             ((SELECT Paths.PathId
               FROM dbo.aspnet_PersonalizationAllUsers AllUsers, dbo.aspnet_Paths Paths
               WHERE Paths.ApplicationId = @ApplicationId
                      AND AllUsers.PathId = Paths.PathId
                      AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
              ) AS SharedDataPerPath
              FULL OUTER JOIN
              (SELECT DISTINCT Paths.PathId
               FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Paths Paths
               WHERE Paths.ApplicationId = @ApplicationId
                      AND PerUser.PathId = Paths.PathId
                      AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
              ) AS UserDataPerPath
              ON SharedDataPerPath.PathId = UserDataPerPath.PathId
             )
        WHERE Paths.PathId = SharedDataPerPath.PathId OR Paths.PathId = UserDataPerPath.PathId
        ORDER BY Paths.Path ASC

        SELECT @TotalRecords = @@ROWCOUNT

        SELECT Paths.Path,
               SharedDataPerPath.LastUpdatedDate,
               SharedDataPerPath.SharedDataLength,
               UserDataPerPath.UserDataLength,
               UserDataPerPath.UserCount
        FROM dbo.aspnet_Paths Paths,
             ((SELECT PageIndex.ItemId AS PathId,
                      AllUsers.LastUpdatedDate AS LastUpdatedDate,
                      DATALENGTH(AllUsers.PageSettings) AS SharedDataLength
               FROM dbo.aspnet_PersonalizationAllUsers AllUsers, #PageIndex PageIndex
               WHERE AllUsers.PathId = PageIndex.ItemId
                     AND PageIndex.IndexId >= @PageLowerBound AND PageIndex.IndexId <= @PageUpperBound
              ) AS SharedDataPerPath
              FULL OUTER JOIN
              (SELECT PageIndex.ItemId AS PathId,
                      SUM(DATALENGTH(PerUser.PageSettings)) AS UserDataLength,
                      COUNT(*) AS UserCount
               FROM aspnet_PersonalizationPerUser PerUser, #PageIndex PageIndex
               WHERE PerUser.PathId = PageIndex.ItemId
                     AND PageIndex.IndexId >= @PageLowerBound AND PageIndex.IndexId <= @PageUpperBound
               GROUP BY PageIndex.ItemId
              ) AS UserDataPerPath
              ON SharedDataPerPath.PathId = UserDataPerPath.PathId
             )
        WHERE Paths.PathId = SharedDataPerPath.PathId OR Paths.PathId = UserDataPerPath.PathId
        ORDER BY Paths.Path ASC
    END
    ELSE
    BEGIN
        -- Insert into our temp table
        INSERT INTO #PageIndex (ItemId)
        SELECT PerUser.Id
        FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths
        WHERE Paths.ApplicationId = @ApplicationId
              AND PerUser.UserId = Users.UserId
              AND PerUser.PathId = Paths.PathId
              AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
              AND (@UserName IS NULL OR Users.LoweredUserName LIKE LOWER(@UserName))
              AND (@InactiveSinceDate IS NULL OR Users.LastActivityDate <= @InactiveSinceDate)
        ORDER BY Paths.Path ASC, Users.UserName ASC

        SELECT @TotalRecords = @@ROWCOUNT

        SELECT Paths.Path, PerUser.LastUpdatedDate, DATALENGTH(PerUser.PageSettings), Users.UserName, Users.LastActivityDate
        FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths, #PageIndex PageIndex
        WHERE PerUser.Id = PageIndex.ItemId
              AND PerUser.UserId = Users.UserId
              AND PerUser.PathId = Paths.PathId
              AND PageIndex.IndexId >= @PageLowerBound AND PageIndex.IndexId <= @PageUpperBound
        ORDER BY Paths.Path ASC, Users.UserName ASC
    END

    RETURN @TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_DeleteAllState]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_DeleteAllState] (
    @AllUsersScope bit,
    @ApplicationName NVARCHAR(256),
    @Count int OUT)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
    BEGIN
        IF (@AllUsersScope = 1)
            DELETE FROM aspnet_PersonalizationAllUsers
            WHERE PathId IN
               (SELECT Paths.PathId
                FROM dbo.aspnet_Paths Paths
                WHERE Paths.ApplicationId = @ApplicationId)
        ELSE
            DELETE FROM aspnet_PersonalizationPerUser
            WHERE PathId IN
               (SELECT Paths.PathId
                FROM dbo.aspnet_Paths Paths
                WHERE Paths.ApplicationId = @ApplicationId)

        SELECT @Count = @@ROWCOUNT
    END
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_SetPageSettings]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationPerUser_SetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @UserName         NVARCHAR(256),
    @Path             NVARCHAR(256),
    @PageSettings     IMAGE,
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER
    DECLARE @UserId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL
    SELECT @UserId = NULL

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        EXEC dbo.aspnet_Paths_CreatePath @ApplicationId, @Path, @PathId OUTPUT
    END

    SELECT @UserId = u.UserId FROM dbo.aspnet_Users u WHERE u.ApplicationId = @ApplicationId AND u.LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
    BEGIN
        EXEC dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, 0, @CurrentTimeUtc, @UserId OUTPUT
    END

    UPDATE   dbo.aspnet_Users WITH (ROWLOCK)
    SET      LastActivityDate = @CurrentTimeUtc
    WHERE    UserId = @UserId
    IF (@@ROWCOUNT = 0) -- Username not found
        RETURN

    IF (EXISTS(SELECT PathId FROM dbo.aspnet_PersonalizationPerUser WHERE UserId = @UserId AND PathId = @PathId))
        UPDATE dbo.aspnet_PersonalizationPerUser SET PageSettings = @PageSettings, LastUpdatedDate = @CurrentTimeUtc WHERE UserId = @UserId AND PathId = @PathId
    ELSE
        INSERT INTO dbo.aspnet_PersonalizationPerUser(UserId, PathId, PageSettings, LastUpdatedDate) VALUES (@UserId, @PathId, @PageSettings, @CurrentTimeUtc)
    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_ResetPageSettings]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationPerUser_ResetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @UserName         NVARCHAR(256),
    @Path             NVARCHAR(256),
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER
    DECLARE @UserId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL
    SELECT @UserId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @UserId = u.UserId FROM dbo.aspnet_Users u WHERE u.ApplicationId = @ApplicationId AND u.LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
    BEGIN
        RETURN
    END

    UPDATE   dbo.aspnet_Users WITH (ROWLOCK)
    SET      LastActivityDate = @CurrentTimeUtc
    WHERE    UserId = @UserId
    IF (@@ROWCOUNT = 0) -- Username not found
        RETURN

    DELETE FROM dbo.aspnet_PersonalizationPerUser WHERE PathId = @PathId AND UserId = @UserId
    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_GetPageSettings]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationPerUser_GetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @UserName         NVARCHAR(256),
    @Path             NVARCHAR(256),
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER
    DECLARE @UserId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL
    SELECT @UserId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @UserId = u.UserId FROM dbo.aspnet_Users u WHERE u.ApplicationId = @ApplicationId AND u.LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
    BEGIN
        RETURN
    END

    UPDATE   dbo.aspnet_Users WITH (ROWLOCK)
    SET      LastActivityDate = @CurrentTimeUtc
    WHERE    UserId = @UserId
    IF (@@ROWCOUNT = 0) -- Username not found
        RETURN

    SELECT p.PageSettings FROM dbo.aspnet_PersonalizationPerUser p WHERE p.PathId = @PathId AND p.UserId = @UserId
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_SetPageSettings]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_SetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @Path             NVARCHAR(256),
    @PageSettings     IMAGE,
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        EXEC dbo.aspnet_Paths_CreatePath @ApplicationId, @Path, @PathId OUTPUT
    END

    IF (EXISTS(SELECT PathId FROM dbo.aspnet_PersonalizationAllUsers WHERE PathId = @PathId))
        UPDATE dbo.aspnet_PersonalizationAllUsers SET PageSettings = @PageSettings, LastUpdatedDate = @CurrentTimeUtc WHERE PathId = @PathId
    ELSE
        INSERT INTO dbo.aspnet_PersonalizationAllUsers(PathId, PageSettings, LastUpdatedDate) VALUES (@PathId, @PageSettings, @CurrentTimeUtc)
    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @Path              NVARCHAR(256))
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    DELETE FROM dbo.aspnet_PersonalizationAllUsers WHERE PathId = @PathId
    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_GetPageSettings]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_GetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @Path              NVARCHAR(256))
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    SELECT p.PageSettings FROM dbo.aspnet_PersonalizationAllUsers p WHERE p.PathId = @PathId
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_SetProperties]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Profile_SetProperties]
    @ApplicationName        nvarchar(256),
    @PropertyNames          ntext,
    @PropertyValuesString   ntext,
    @PropertyValuesBinary   image,
    @UserName               nvarchar(256),
    @IsUserAnonymous        bit,
    @CurrentTimeUtc         datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
       BEGIN TRANSACTION
       SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    DECLARE @UserId uniqueidentifier
    DECLARE @LastActivityDate datetime
    SELECT  @UserId = NULL
    SELECT  @LastActivityDate = @CurrentTimeUtc

    SELECT @UserId = UserId
    FROM   dbo.aspnet_Users
    WHERE  ApplicationId = @ApplicationId AND LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
        EXEC dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, @IsUserAnonymous, @LastActivityDate, @UserId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    UPDATE dbo.aspnet_Users
    SET    LastActivityDate=@CurrentTimeUtc
    WHERE  UserId = @UserId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF (EXISTS( SELECT *
               FROM   dbo.aspnet_Profile
               WHERE  UserId = @UserId))
        UPDATE dbo.aspnet_Profile
        SET    PropertyNames=@PropertyNames, PropertyValuesString = @PropertyValuesString,
               PropertyValuesBinary = @PropertyValuesBinary, LastUpdatedDate=@CurrentTimeUtc
        WHERE  UserId = @UserId
    ELSE
        INSERT INTO dbo.aspnet_Profile(UserId, PropertyNames, PropertyValuesString, PropertyValuesBinary, LastUpdatedDate)
             VALUES (@UserId, @PropertyNames, @PropertyValuesString, @PropertyValuesBinary, @CurrentTimeUtc)

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
    	SET @TranStarted = 0
    	COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetProperties]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Profile_GetProperties]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @CurrentTimeUtc       datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN

    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL

    SELECT @UserId = UserId
    FROM   dbo.aspnet_Users
    WHERE  ApplicationId = @ApplicationId AND LoweredUserName = LOWER(@UserName)

    IF (@UserId IS NULL)
        RETURN
    SELECT TOP 1 PropertyNames, PropertyValuesString, PropertyValuesBinary
    FROM         dbo.aspnet_Profile
    WHERE        UserId = @UserId

    IF (@@ROWCOUNT > 0)
    BEGIN
        UPDATE dbo.aspnet_Users
        SET    LastActivityDate=@CurrentTimeUtc
        WHERE  UserId = @UserId
    END
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetProfiles]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Profile_GetProfiles]
    @ApplicationName        nvarchar(256),
    @ProfileAuthOptions     int,
    @PageIndex              int,
    @PageSize               int,
    @UserNameToMatch        nvarchar(256) = NULL,
    @InactiveSinceDate      datetime      = NULL
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
        SELECT  u.UserId
        FROM    dbo.aspnet_Users u, dbo.aspnet_Profile p
        WHERE   ApplicationId = @ApplicationId
            AND u.UserId = p.UserId
            AND (@InactiveSinceDate IS NULL OR LastActivityDate <= @InactiveSinceDate)
            AND (     (@ProfileAuthOptions = 2)
                   OR (@ProfileAuthOptions = 0 AND IsAnonymous = 1)
                   OR (@ProfileAuthOptions = 1 AND IsAnonymous = 0)
                 )
            AND (@UserNameToMatch IS NULL OR LoweredUserName LIKE LOWER(@UserNameToMatch))
        ORDER BY UserName

    SELECT  u.UserName, u.IsAnonymous, u.LastActivityDate, p.LastUpdatedDate,
            DATALENGTH(p.PropertyNames) + DATALENGTH(p.PropertyValuesString) + DATALENGTH(p.PropertyValuesBinary)
    FROM    dbo.aspnet_Users u, dbo.aspnet_Profile p, #PageIndexForUsers i
    WHERE   u.UserId = p.UserId AND p.UserId = i.UserId AND i.IndexId >= @PageLowerBound AND i.IndexId <= @PageUpperBound

    SELECT COUNT(*)
    FROM   #PageIndexForUsers

    DROP TABLE #PageIndexForUsers
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]
    @ApplicationName        nvarchar(256),
    @ProfileAuthOptions     int,
    @InactiveSinceDate      datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
    BEGIN
        SELECT 0
        RETURN
    END

    SELECT  COUNT(*)
    FROM    dbo.aspnet_Users u, dbo.aspnet_Profile p
    WHERE   ApplicationId = @ApplicationId
        AND u.UserId = p.UserId
        AND (LastActivityDate <= @InactiveSinceDate)
        AND (
                (@ProfileAuthOptions = 2)
                OR (@ProfileAuthOptions = 0 AND IsAnonymous = 1)
                OR (@ProfileAuthOptions = 1 AND IsAnonymous = 0)
            )
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_DeleteProfiles]    Script Date: 05/23/2013 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Profile_DeleteProfiles]
    @ApplicationName        nvarchar(256),
    @UserNames              nvarchar(4000)
AS
BEGIN
    DECLARE @UserName     nvarchar(256)
    DECLARE @CurrentPos   int
    DECLARE @NextPos      int
    DECLARE @NumDeleted   int
    DECLARE @DeletedUser  int
    DECLARE @TranStarted  bit
    DECLARE @ErrorCode    int

    SET @ErrorCode = 0
    SET @CurrentPos = 1
    SET @NumDeleted = 0
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
        BEGIN TRANSACTION
        SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    WHILE (@CurrentPos <= LEN(@UserNames))
    BEGIN
        SELECT @NextPos = CHARINDEX(N',', @UserNames,  @CurrentPos)
        IF (@NextPos = 0 OR @NextPos IS NULL)
            SELECT @NextPos = LEN(@UserNames) + 1

        SELECT @UserName = SUBSTRING(@UserNames, @CurrentPos, @NextPos - @CurrentPos)
        SELECT @CurrentPos = @NextPos+1

        IF (LEN(@UserName) > 0)
        BEGIN
            SELECT @DeletedUser = 0
            EXEC dbo.aspnet_Users_DeleteUser @ApplicationName, @UserName, 4, @DeletedUser OUTPUT
            IF( @@ERROR <> 0 )
            BEGIN
                SET @ErrorCode = -1
                GOTO Cleanup
            END
            IF (@DeletedUser <> 0)
                SELECT @NumDeleted = @NumDeleted + 1
        END
    END
    SELECT @NumDeleted
    IF (@TranStarted = 1)
    BEGIN
    	SET @TranStarted = 0
    	COMMIT TRANSACTION
    END
    SET @TranStarted = 0

    RETURN 0

Cleanup:
    IF (@TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END
    RETURN @ErrorCode
END
GO
/****** Object:  Default [DF__aspnet_Ap__Appli__08EA5793]    Script Date: 05/23/2013 15:14:34 ******/
ALTER TABLE [dbo].[aspnet_Applications] ADD  DEFAULT (newid()) FOR [ApplicationId]
GO
/****** Object:  Default [DF__aspnet_Us__UserI__0EA330E9]    Script Date: 05/23/2013 15:14:38 ******/
ALTER TABLE [dbo].[aspnet_Users] ADD  DEFAULT (newid()) FOR [UserId]
GO
/****** Object:  Default [DF__aspnet_Us__Mobil__0F975522]    Script Date: 05/23/2013 15:14:38 ******/
ALTER TABLE [dbo].[aspnet_Users] ADD  DEFAULT (NULL) FOR [MobileAlias]
GO
/****** Object:  Default [DF__aspnet_Us__IsAno__108B795B]    Script Date: 05/23/2013 15:14:38 ******/
ALTER TABLE [dbo].[aspnet_Users] ADD  DEFAULT ((0)) FOR [IsAnonymous]
GO
/****** Object:  Default [DF__aspnet_Pa__PathI__5BE2A6F2]    Script Date: 05/23/2013 15:14:38 ******/
ALTER TABLE [dbo].[aspnet_Paths] ADD  DEFAULT (newid()) FOR [PathId]
GO
/****** Object:  Default [DF__aspnet_Ro__RoleI__44FF419A]    Script Date: 05/23/2013 15:14:38 ******/
ALTER TABLE [dbo].[aspnet_Roles] ADD  DEFAULT (newid()) FOR [RoleId]
GO
/****** Object:  Default [DF__aspnet_Perso__Id__6754599E]    Script Date: 05/23/2013 15:14:38 ******/
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] ADD  DEFAULT (newid()) FOR [Id]
GO
/****** Object:  Default [DF__aspnet_Me__Passw__239E4DCF]    Script Date: 05/23/2013 15:14:38 ******/
ALTER TABLE [dbo].[aspnet_Membership] ADD  DEFAULT ((0)) FOR [PasswordFormat]
GO
/****** Object:  Default [DF_OfficeTime_TimeId]    Script Date: 05/23/2013 15:14:38 ******/
ALTER TABLE [dbo].[OfficeTime] ADD  CONSTRAINT [DF_OfficeTime_TimeId]  DEFAULT (newid()) FOR [OfficeTimeId]
GO
/****** Object:  ForeignKey [FK__aspnet_Us__Appli__0DAF0CB0]    Script Date: 05/23/2013 15:14:38 ******/
ALTER TABLE [dbo].[aspnet_Users]  WITH CHECK ADD FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
/****** Object:  ForeignKey [FK__aspnet_Pa__Appli__5AEE82B9]    Script Date: 05/23/2013 15:14:38 ******/
ALTER TABLE [dbo].[aspnet_Paths]  WITH CHECK ADD FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
/****** Object:  ForeignKey [FK__aspnet_Ro__Appli__440B1D61]    Script Date: 05/23/2013 15:14:38 ******/
ALTER TABLE [dbo].[aspnet_Roles]  WITH CHECK ADD FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__PathI__68487DD7]    Script Date: 05/23/2013 15:14:38 ******/
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser]  WITH CHECK ADD FOREIGN KEY([PathId])
REFERENCES [dbo].[aspnet_Paths] ([PathId])
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__UserI__693CA210]    Script Date: 05/23/2013 15:14:38 ******/
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
/****** Object:  ForeignKey [FK__aspnet_Pr__UserI__38996AB5]    Script Date: 05/23/2013 15:14:38 ******/
ALTER TABLE [dbo].[aspnet_Profile]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
/****** Object:  ForeignKey [FK__aspnet_Me__Appli__21B6055D]    Script Date: 05/23/2013 15:14:38 ******/
ALTER TABLE [dbo].[aspnet_Membership]  WITH CHECK ADD FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
/****** Object:  ForeignKey [FK__aspnet_Me__UserI__22AA2996]    Script Date: 05/23/2013 15:14:38 ******/
ALTER TABLE [dbo].[aspnet_Membership]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__PathI__628FA481]    Script Date: 05/23/2013 15:14:38 ******/
ALTER TABLE [dbo].[aspnet_PersonalizationAllUsers]  WITH CHECK ADD FOREIGN KEY([PathId])
REFERENCES [dbo].[aspnet_Paths] ([PathId])
GO
/****** Object:  ForeignKey [FK__aspnet_Us__RoleI__4AB81AF0]    Script Date: 05/23/2013 15:14:38 ******/
ALTER TABLE [dbo].[aspnet_UsersInRoles]  WITH CHECK ADD FOREIGN KEY([RoleId])
REFERENCES [dbo].[aspnet_Roles] ([RoleId])
GO
/****** Object:  ForeignKey [FK__aspnet_Us__UserI__49C3F6B7]    Script Date: 05/23/2013 15:14:38 ******/
ALTER TABLE [dbo].[aspnet_UsersInRoles]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
/****** Object:  ForeignKey [FK_OfficeTime_aspnet_Users]    Script Date: 05/23/2013 15:14:38 ******/
ALTER TABLE [dbo].[OfficeTime]  WITH CHECK ADD  CONSTRAINT [FK_OfficeTime_aspnet_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
ALTER TABLE [dbo].[OfficeTime] CHECK CONSTRAINT [FK_OfficeTime_aspnet_Users]
GO
