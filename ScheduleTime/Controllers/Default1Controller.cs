﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ScheduleTime.Models;

namespace ScheduleTime.Controllers
{ 
    public class Default1Controller : Controller
    {
        private ScheduleTimeDB db = new ScheduleTimeDB();

        //
        // GET: /Default1/

        public ViewResult Index()
        {
            var officetime = db.OfficeTime.Include(o => o.aspnet_Users);
            return View(officetime.ToList());
        }

        //
        // GET: /Default1/Details/5

        public ViewResult Details(Guid id)
        {
            OfficeTime officetime = db.OfficeTime.Find(id);
            return View(officetime);
        }

        //
        // GET: /Default1/Create

        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.aspnet_Users, "UserId", "UserName");
            return View();
        } 

        //
        // POST: /Default1/Create

        [HttpPost]
        public ActionResult Create(OfficeTime officetime)
        {
            if (ModelState.IsValid)
            {
                officetime.OfficeTimeId = Guid.NewGuid();
                db.OfficeTime.Add(officetime);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.UserId = new SelectList(db.aspnet_Users, "UserId", "UserName", officetime.UserId);
            return View(officetime);
        }
        
        //
        // GET: /Default1/Edit/5
 
        public ActionResult Edit(Guid id)
        {
            OfficeTime officetime = db.OfficeTime.Find(id);
            ViewBag.UserId = new SelectList(db.aspnet_Users, "UserId", "UserName", officetime.UserId);
            return View(officetime);
        }

        //
        // POST: /Default1/Edit/5

        [HttpPost]
        public ActionResult Edit(OfficeTime officetime)
        {
            if (ModelState.IsValid)
            {
                db.Entry(officetime).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.aspnet_Users, "UserId", "UserName", officetime.UserId);
            return View(officetime);
        }

        //
        // GET: /Default1/Delete/5
 
        public ActionResult Delete(Guid id)
        {
            OfficeTime officetime = db.OfficeTime.Find(id);
            return View(officetime);
        }

        //
        // POST: /Default1/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(Guid id)
        {            
            OfficeTime officetime = db.OfficeTime.Find(id);
            db.OfficeTime.Remove(officetime);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}