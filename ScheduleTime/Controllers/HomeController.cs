﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ScheduleTime.Models;
using System.Web.Security;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Globalization;
using System.IO; 

namespace ScheduleTime.Controllers
{
    public class HomeController : Controller
    {
        ScheduleTimeDB db = new ScheduleTimeDB();

        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult Time()
        {
            MembershipUser member = Membership.GetUser();
            Guid id = new Guid(member.ProviderUserKey.ToString());
            List<OfficeTime> ot = db.OfficeTime.Where(x => x.UserId.Equals(id)).ToList();
            return View(ot.Where(x => x.ActionTime.Month.Equals(DateTime.Now.Month)).Where(x => x.ActionTime.Day.Equals(DateTime.Now.Day)).OrderBy(x => x.ActionTime));
        }

        [Authorize]
        public ActionResult TiempoDiaActual()
        {
            MembershipUser member = Membership.GetUser();
            Guid id = new Guid(member.ProviderUserKey.ToString());
            List<OfficeTime> ot = db.OfficeTime.Where(x => x.UserId.Equals(id)).ToList();
            return View(ot.Where(x => x.ActionTime.Month.Equals(DateTime.Now.Month)).Where(x => x.ActionTime.Day.Equals(DateTime.Now.Day)).OrderBy(x => x.ActionTime));
        }

        [Authorize]
        public ActionResult TiempoMesActual()
        {
            MembershipUser member = Membership.GetUser();
            Guid id = new Guid(member.ProviderUserKey.ToString());
            List<OfficeTime> ot = db.OfficeTime.Where(x => x.UserId.Equals(id)).ToList();
            return View(ot.Where(x => x.ActionTime.Month.Equals(DateTime.Now.Month)).Where(x => !x.ActionTime.Day.Equals(DateTime.Now.Day)).OrderBy(x => x.ActionTime));
        }

        [Authorize]
        public ActionResult PorMes()
        {
            List<string> monthNames = DateTimeFormatInfo.CurrentInfo.MonthNames.Take(12).ToList();
            List<SelectListItem> meses = (from c in monthNames                        
                                          select new SelectListItem () { Text = c, Value = c, Selected = false }).ToList();
            int i=0;
            foreach (var mes in meses)
            {
                meses[i].Value = (i + 1).ToString(CultureInfo.InvariantCulture);
                i++;
            }
            meses.Insert(0, new SelectListItem() { Text = "--Seleccione--", Value = "-1", Selected = false });
            return View(meses);
        }

        [Authorize]
        [HttpPost]
        public ActionResult PorMes(int mes)
        {
            MembershipUser member = Membership.GetUser();
            Guid id = new Guid(member.ProviderUserKey.ToString());
            List<OfficeTime> ot = db.OfficeTime.Where(x => x.UserId.Equals(id)).ToList();
            var tal = ot.Where(x => x.ActionTime.Month.Equals(mes)).OrderBy(x => x.ActionTime);
            
            return PartialView("_Mes", tal);
        }

        [HttpPost]
        public ActionResult exportToExcel()
        {
            try
            {
                Excel.Application xlApp;
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;

                xlApp = new Excel.Application();
                xlWorkBook = xlApp.Workbooks.Add(misValue);

                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                MembershipUser member = Membership.GetUser();
                Guid id = new Guid(member.ProviderUserKey.ToString());
                List<OfficeTime> ot = db.OfficeTime.Where(x => x.UserId.Equals(id)).OrderBy(x => x.ActionTime).ToList();

                int i = 1;
                foreach (var officeTime in ot)
                {
                    //xlWorkSheet.Cells[i, 1] = "qwe";
                    xlWorkSheet.Cells[i, 1] = officeTime.ActionTime;
                    xlWorkSheet.Cells[i, 2] = officeTime.UserId.ToString();
                    xlWorkSheet.Cells[i, 3] = officeTime.Enter;
                    i++;
                }

                xlWorkBook.SaveAs(@"c:\Horarios.xls", Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();

                releaseObject(xlWorkSheet);
                releaseObject(xlWorkBook);
                releaseObject(xlApp);

                //MessageBox.Show("Excel file created , you can find the file c:\\csharp-Excel.xls");

                return Json(new { data = "Archivo creado" });
            }
            catch (Exception ex)
            {
                return Json(new { data = ex.Message });
            }
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                //MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult ActionTime(string entrada)
        {
            bool esEntrada = Convert.ToBoolean(entrada);
            MembershipUser member = Membership.GetUser();
            Guid id = new Guid(member.ProviderUserKey.ToString());
            List<OfficeTime> ot = db.OfficeTime.Where(x => x.UserId.Equals(id)).OrderBy(x => x.ActionTime).ToList();
                        
            if ((ot.Count > 0) && (!ot.LastOrDefault().Enter) && (!esEntrada))
            {
                return Json(new { data = "Data no ingresada, debes marcar una entrada primero..." });
            }
            if ((ot.Count > 0) && (ot.LastOrDefault().Enter) && (esEntrada))
            {
                return Json(new { data = "Data no ingresada, debes marcar una salida primero..." });
            }            
            else
            {
                OfficeTime officeTime = new OfficeTime();

                if (IsBetween(DateTime.Now, new DateTime(1, 1, 1, 12, 0, 0), new DateTime(1, 1, 1, 12, 59, 0)))
                {
                    if (esEntrada)
                    {
                        officeTime.ActionTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 13, 0, 0);
                    }
                    else
                    {
                        officeTime.ActionTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 12, 0, 0);
                    }
                }
                else
                {
                    officeTime.ActionTime = DateTime.Now;
                }

                officeTime.OfficeTimeId = Guid.NewGuid();
                officeTime.UserId = new Guid(Membership.GetUser().ProviderUserKey.ToString());
                officeTime.Enter = esEntrada;
                db.OfficeTime.Add(officeTime);
                db.SaveChanges();

                return Json(new { data = "Registro ingresado..." });
            }
        }

        [Authorize]
        public ActionResult Create()
        {            
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Create(OfficeTime officetime)
        {
            officetime.UserId = new Guid(Membership.GetUser().ProviderUserKey.ToString());
            if (ModelState.IsValid)
            {
                if (IsBetween(officetime.ActionTime, new DateTime(1, 1, 1, 12, 0, 0), new DateTime(1, 1, 1, 12, 59, 0)))
                {
                    if (officetime.Enter)
                    {
                        officetime.ActionTime = new DateTime(officetime.ActionTime.Year, officetime.ActionTime.Month, officetime.ActionTime.Day, 13, 0, 0);
                    }
                    else
                    {
                        officetime.ActionTime = new DateTime(officetime.ActionTime.Year, officetime.ActionTime.Month, officetime.ActionTime.Day, 12, 0, 0);
                    }
                }

                officetime.OfficeTimeId = Guid.NewGuid();
                db.OfficeTime.Add(officetime);
                db.SaveChanges();
                return RedirectToAction("Create");
            }

            ViewBag.UserId = new SelectList(db.aspnet_Users, "UserId", "UserName", officetime.UserId);
            return View(officetime);
        }
        
        [Authorize]
        public ActionResult Edit(Guid id)
        {
            OfficeTime officetime = db.OfficeTime.Find(id);
            ViewBag.UserId = new SelectList(db.aspnet_Users, "UserId", "UserName", officetime.UserId);
            return View(officetime);
        }

        //
        // POST: /Default1/Edit/5

        [Authorize]
        [HttpPost]
        public ActionResult Edit(OfficeTime officetime)
        {            
            if (ModelState.IsValid)
            {
                officetime.UserId = new Guid(Membership.GetUser().ProviderUserKey.ToString());
                db.Entry(officetime).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.aspnet_Users, "UserId", "UserName", officetime.UserId);
            return View(officetime);
        }

        /// <summary>
        /// Calcula el in between de la hora del medio dia que debe descontarse
        /// </summary>
        /// <param name="input"></param>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns></returns>
        private static bool IsBetween(DateTime input, DateTime date1, DateTime date2)
        {
            if (input.Hour == 12)
            {
                return (input.Minute > date1.Minute && input.Minute < date2.Minute);
            }
            else
            {
                return false;
            }

        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            try
            {
                OfficeTime officetime = db.OfficeTime.Find(id);
                db.OfficeTime.Remove(officetime);
                db.SaveChanges();
                return Json(new { data = "Registro borrado..." });
            }
            catch (Exception ex)
            {
                return Json(new { data = "Ha ocurrido un Eror: " + ex.Message });
            }
            
        }
        
        [Authorize]
        public ActionResult About()
        {
            return View();
        }

        [Authorize]
        public ActionResult Contact()
        {
            return View();
        }
                
        public ActionResult Gallery()
        {
            string _rute = Path.Combine(Server.MapPath(Request.ApplicationPath), @"Content\img\bowling20-09-13\");

            DirectoryInfo files = new DirectoryInfo(_rute);
            var imgFiles = files.GetFiles("*.jpg");

            List<content_image> listado = new List<content_image>();
            content_image myImage = null;

            for (int i = 0; i < imgFiles.Length; i++)
            {
                myImage = new content_image(imgFiles[i].Name, Path.Combine(@"~\Content\img\bowling20-09-13\", imgFiles[i].Name));
                listado.Add(myImage);
            }

            //~/Content/img

            return View(listado.OrderBy(x => x.Name));
            //return View();
        }

        public ActionResult MasonryGallery()
        {
            string _rute = Path.Combine(Server.MapPath(Request.ApplicationPath), @"Content\img\bowling20-09-13\");

            DirectoryInfo files = new DirectoryInfo(_rute);
            var imgFiles = files.GetFiles("*.jpg");

            List<content_image> listado = new List<content_image>();
            content_image myImage = null;

            for (int i = 0; i < imgFiles.Length; i++)
            {
                myImage = new content_image(imgFiles[i].Name, Path.Combine(@"~\Content\img\bowling20-09-13\", imgFiles[i].Name));
                listado.Add(myImage);
            }

            //~/Content/img

            return View(listado.OrderBy(x => x.Name));
            //return View();
        }

        public ActionResult Projects()
        {
            return View();
        }
    }
}
