﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScheduleTime.Models
{
    public class content_image
    {
        public string Name { get; set; }
        public string FileRoute { get; set; }

        public content_image(string _name, string _fileRoute)
        {
            this.Name = _name;
            this.FileRoute = _fileRoute;
        }
    }
}