﻿$(document).ready(initialize);
//$(window).load(initialize);

function initialize() {
    $(document).tooltip();
    $('.btn-inverse').click(function () {
        $('input.btnClear').empty();

        var id = $(this).attr('id');
        var btnLink = "<input id='" + id + "' type='button' class='btn btn-primary btnClear' value='Si'/>";

        $(btnLink).appendTo($('div.modal-footer'));
        $('.modal').modal('show');
        $('.modal').on('shown', function () {
            $('input.btnClear').click(function () {
                var _id = $(this).attr('id');
                $.ajax({
                    url: "Delete",
                    type: "post",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ id: _id }),
                    success: function (data) {
                        //alert(data.data);
                        window.location.href = 'TiempoDiaActual';
                    },
                    error: function () {
                        alert("error");
                    }
                });
            });
        })
    });

    $(".horaPromedio").each(function (index) {
        if (dateCompare($(this).attr("title"), "08:00:00") >= 0) {
            $(this).css("color", "green");
        }
    });
}

function dateCompare(time1, time2) {
    var t1 = new Date();
    var parts = time1.split(":");
    t1.setHours(parts[0], parts[1], parts[2], 0);

    var t2 = new Date();
    parts = time2.split(":");
    t2.setHours(parts[0], parts[1], parts[2], 0);

    // returns 1 if greater, -1 if less and 0 if the same
    if (t1.getTime() >= t2.getTime()) return 1;
    if (t1.getTime() < t2.getTime()) return -1;
}
