﻿$(document).ready(initialize);
//$(window).load(initialize);

function initialize() {
    $('#out').click(function () {
        save("false");
    })
    $('#in').click(function () {
        save("true");
    })
}

function save(_entrada) {
    $.ajax({
        url: "Home/ActionTime",
        type: "post",        
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ entrada: _entrada }),        
        success: function (data) {
            alert(data.data);
        },
        error: function () {
            alert("error");
        }
    });
}