﻿/// <reference path="jquery-1.5.1-vsdoc.js" />
$(document).ready(initialize);
function initialize() {
    $(document).tooltip();

    $('#exportToExcel').click(function () {
        $.ajax({
            url: "exportToExcel",
            type: "post",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                alert(data.data);
            },
            error: function (data) {
                alert(data.data);
            }
        });
    });
}